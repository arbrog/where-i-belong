import { makeStyles, Theme, Typography, Grid, TextField, Divider } from '@material-ui/core';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../state/rootReducer';
import SearchIcon from '@material-ui/icons/Search';
import { ProvidePageTitle } from '../base/title';
import { SearchForAssets } from '../asset/state/actions';
import AssetCard from '../asset/components/assetCard';
import SearchAsset from '../asset/components/searchAsset';
import debounce from 'lodash.debounce';

const useStyles = makeStyles((theme: Theme) => ({
    loaderRoot: {
        display: 'flex',
        justifyContent: 'center',
        width: '100%',
    },
    mainTitle: { marginBottom: 12, color: theme.palette.text.secondary },
    subTitle: { color: theme.palette.text.secondary, fontStyle: 'italic' },
    container: {
        padding: theme.spacing(3),
    },
    progress: {
        margin: theme.spacing(2),
    },
    mainContent: {
        minWidth: theme.spacing(50),
    },
    search: {
        minWidth: theme.spacing(25),
    },
    divider: { width: '100%', marginBottom: theme.spacing(2), marginTop: theme.spacing(2) },
}));

const Dashboard: React.FunctionComponent = () => {
    const classes = useStyles({});
    const dispatch = useDispatch();
    const { searchAssets } = useSelector((state: RootState) => state.asset);
    const [assetName, setAssetName] = React.useState('');
    const [onlyOwned, setOnlyOwned] = React.useState(false);
    const [metadataSearch, setMetadataSearch] = React.useState('');
    const [metaSearchHelpText, setMetaSearchHelpText] = React.useState('');
    const [metaSearchError, setMetaSearchError] = React.useState(false);

    const debouncedsetAssetName = React.useMemo(() => debounce(setAssetName, 300), [setAssetName]);

    const debouncedsetMetadataSearch = React.useMemo(() => debounce(setMetadataSearch, 300), [setMetadataSearch]);

    React.useEffect(() => {
        setMetaSearchHelpText('');
        setMetaSearchError(false);
        // let meta = undefined;
        // if (metadataSearch !== '') {
        //     try {
        //         meta = JSON.parse(metadataSearch);
        //     } catch (error) {
        //         setMetaSearchHelpText(error.toString());
        //         setMetaSearchError(true);
        //     }
        // }
        dispatch(SearchForAssets({ name: assetName, metadata: null, only_owned: onlyOwned }));
    }, [dispatch, assetName, metadataSearch, onlyOwned]);

    return (
        <div className={classes.container}>
            <ProvidePageTitle>Search Assets</ProvidePageTitle>
            <Grid container spacing={3}>
                <Grid item xs={12}>
                    <Typography variant="h5" className={classes.mainTitle}>
                        Search Assets
                    </Typography>
                    <Typography className={classes.subTitle}>Shows 25 most recent modified assets with the ability to search through all assets by name</Typography>
                </Grid>
            </Grid>
            <Grid container spacing={1} className={classes.mainContent} alignItems="flex-start" direction="column">
                <Grid item style={{ minWidth: 'inherit' }}>
                    <SearchAsset setAssetName={debouncedsetAssetName} onlyOwned={onlyOwned} setOnlyOwned={setOnlyOwned} />
                </Grid>
                <Grid item style={{ minWidth: 'inherit' }}>
                    <Grid container spacing={1} alignItems="flex-end">
                        <Grid item>
                            <SearchIcon />
                        </Grid>
                        <Grid item>
                            <TextField
                                label="Search Metadata (Exact)"
                                onChange={(event) => debouncedsetMetadataSearch(event.target.value)}
                                error={metaSearchError}
                                helperText={metaSearchHelpText}
                                className={classes.search}
                            />
                        </Grid>
                        <Grid item>{`Ex: {"key1": "value1", "key2": "value2"}`}</Grid>
                    </Grid>
                </Grid>
            </Grid>
            <Divider className={classes.divider} />
            <Grid container justify="center" spacing={4}>
                {searchAssets
                    ? searchAssets.map((asset) => {
                          return <AssetCard asset={asset} key={asset.id} />;
                      })
                    : null}
            </Grid>
        </div>
    );
};

export default Dashboard;
