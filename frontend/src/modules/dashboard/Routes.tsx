import React from 'react';
import { Route } from 'react-router-dom';
import { ensureAuthenticated } from '../auth/wrapper';

const Dashboard = React.lazy(() => import('./Dashboard'));

export const DashboardRoutes = (): React.ReactNode[] => [<Route key="searchAssets" path="/searchAssets" component={ensureAuthenticated(Dashboard)} />];
