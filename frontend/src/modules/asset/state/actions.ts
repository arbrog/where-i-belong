import {
    SearchAssetsQuery,
    SearchAssetsQueryVariables,
    SearchAssets,
    GetAssetQueryVariables,
    GetAssetQuery,
    GetAsset,
    CreateAssetMutationVariables,
    CreateAssetMutation,
    CreateAsset,
    ModifyAssetMutationVariables,
    ModifyAssetMutation,
    ModifyAsset,
    DeleteAssetMutationVariables,
    DeleteAssetMutation,
    DeleteAsset,
    GetAssetsQuery,
    GetAssets,
    GetAssetsQueryVariables,
    UpsertAssetPermissionMutation,
    UpsertAssetPermissionMutationVariables,
    UpsertAssetPermission,
    DeleteAssetPermissionMutation,
    DeleteAssetPermission,
    DeleteAssetPermissionMutationVariables,
    SetAssetTagsMutation,
    SetAssetTagsMutationVariables,
    SetAssetTags,
} from '../../../generated/graphql';
import { actionCreator, ActionTypeHelper } from '../../../state/utils';
import { WrapLoading } from '../../../state/loading/actions';
import { GetGraphQLClient } from '../../auth/state/actions';
import { enqueueSnackbar } from '../../../state/snack/actions';
import { IAsset } from './reducer';

export const SearchAssetsActionCreator = actionCreator<'SEARCH_ASSETS', SearchAssetsQuery['searchAssets']>('SEARCH_ASSETS');
export const GetAssetActionCreator = actionCreator<'GET_ASSET', GetAssetQuery['getAsset']>('GET_ASSET');
export const GetAssetsActionCreator = actionCreator<'GET_ASSETS', GetAssetsQuery['getAssets']>('GET_ASSETS');
export const CreateAssetActionCreator = actionCreator<'CREATE_ASSET', CreateAssetMutation['addAsset']>('CREATE_ASSET');
export const EditAssetActionCreator = actionCreator<'EDIT_ASSET', ModifyAssetMutation['editAsset']>('EDIT_ASSET');
export const SetAssetTagsActionCreator = actionCreator<'SET_ASSET_TAGS', SetAssetTagsMutation['setAssetTags']>('SET_ASSET_TAGS');
export const DeleteAssetActionCreator = actionCreator<'DELETE_ASSET', DeleteAssetMutation['deleteAsset']>('DELETE_ASSET');
export const UpsertAssetPermissionActionCreator = actionCreator<'UPSERT_ASSET_PERMISSION', UpsertAssetPermissionMutation['upsertAssetPermission']>('UPSERT_ASSET_PERMISSION');
export const DeleteAssetPermissionActionCreator = actionCreator<'DELETE_ASSET_PERMISSION', DeleteAssetPermissionMutation['deleteAssetPermission']>('DELETE_ASSET_PERMISSION');

export type AssetActions =
    | ReturnType<typeof SearchAssetsActionCreator>
    | ReturnType<typeof GetAssetActionCreator>
    | ReturnType<typeof EditAssetActionCreator>
    | ReturnType<typeof DeleteAssetActionCreator>
    | ReturnType<typeof CreateAssetActionCreator>
    | ReturnType<typeof UpsertAssetPermissionActionCreator>
    | ReturnType<typeof DeleteAssetPermissionActionCreator>
    | ReturnType<typeof GetAssetsActionCreator>
    | ReturnType<typeof SetAssetTagsActionCreator>;

export const SEARCH_ASSETS_LOADING_INDICATOR = 'asset_search';

export function GetAllAssets(args: GetAssetsQueryVariables): ActionTypeHelper<Promise<void>> {
    return WrapLoading(SEARCH_ASSETS_LOADING_INDICATOR, async (dispatch, getState) => {
        const client = GetGraphQLClient()(dispatch, getState, undefined);
        const response = await client.query<GetAssetsQuery, GetAssetsQueryVariables>({
            query: GetAssets,
            variables: args,
        });

        if (response.errors) {
            throw new Error(response.errors.map((e) => e.message).join('\n'));
        }
        const { getAssets } = response.data;

        dispatch(GetAssetsActionCreator(getAssets));
    });
}

export const GET_ASSETS_LOADING_INDICATOR = 'get_assets';

export function SearchForAssets(args: SearchAssetsQueryVariables): ActionTypeHelper<Promise<void>> {
    return WrapLoading(GET_ASSETS_LOADING_INDICATOR, async (dispatch, getState) => {
        const client = GetGraphQLClient()(dispatch, getState, undefined);
        const response = await client.query<SearchAssetsQuery, SearchAssetsQueryVariables>({
            query: SearchAssets,
            variables: args,
        });

        if (response.errors) {
            throw new Error(response.errors.map((e) => e.message).join('\n'));
        }
        const { searchAssets } = response.data;

        dispatch(SearchAssetsActionCreator(searchAssets));
    });
}

export const FETCH_ASSET_LOADING_INDICATOR = 'asset_loading';

export function FetchAsset(args: GetAssetQueryVariables): ActionTypeHelper<Promise<void>> {
    return WrapLoading(FETCH_ASSET_LOADING_INDICATOR, async (dispatch, getState) => {
        const client = GetGraphQLClient()(dispatch, getState, undefined);
        const response = await client.query<GetAssetQuery, GetAssetQueryVariables>({
            query: GetAsset,
            variables: args,
        });

        if (response.errors) {
            throw new Error(response.errors.map((e) => e.message).join('\n'));
        }
        const { getAsset } = response.data;

        dispatch(GetAssetActionCreator(getAsset));
    });
}

export const GET_ASSET_LOADING_INDICATOR = 'get_asset_loading';

export function GetAssetHelper(args: GetAssetQueryVariables): ActionTypeHelper<Promise<IAsset>> {
    return WrapLoading(GET_ASSET_LOADING_INDICATOR, async (dispatch, getState) => {
        const client = GetGraphQLClient()(dispatch, getState, undefined);
        const response = await client.query<GetAssetQuery, GetAssetQueryVariables>({
            query: GetAsset,
            variables: args,
        });

        if (response.errors) {
            throw new Error(response.errors.map((e) => e.message).join('\n'));
        }
        const { getAsset } = response.data;

        return getAsset;
    });
}

export const CREATE_ASSET_LOADING = 'create_asset_loading';

export function CreateAssetCall(args: CreateAssetMutationVariables): ActionTypeHelper<Promise<CreateAssetMutation['addAsset'] | undefined>> {
    return WrapLoading(CREATE_ASSET_LOADING, async (dispatch, getState) => {
        const client = GetGraphQLClient()(dispatch, getState, undefined);
        const response = await client.mutate<CreateAssetMutation, CreateAssetMutationVariables>({
            mutation: CreateAsset,
            variables: args,
        });

        if (response.errors) {
            throw new Error(response.errors.map((e) => e.message).join('\n'));
        }
        dispatch(CreateAssetActionCreator());
        dispatch(enqueueSnackbar({ message: 'Successfully added asset', options: { variant: 'success' } }));
        return response.data?.addAsset;
    });
}

export const EDIT_ASSET_LOADING = 'edit_asset_loading';

export function EditAssetCall(args: ModifyAssetMutationVariables): ActionTypeHelper<Promise<boolean>> {
    return WrapLoading(EDIT_ASSET_LOADING, async (dispatch, getState) => {
        const client = GetGraphQLClient()(dispatch, getState, undefined);
        const response = await client.mutate<ModifyAssetMutation, ModifyAssetMutationVariables>({
            mutation: ModifyAsset,
            variables: args,
        });

        if (response.errors) {
            throw new Error(response.errors.map((e) => e.message).join('\n'));
        }
        dispatch(EditAssetActionCreator());
        dispatch(enqueueSnackbar({ message: 'Successfully modified asset', options: { variant: 'success' } }));
        return true;
    });
}

export const SET_ASSET_TAGS_LOADING = 'set_asset_tags_loading';

export function SetAssetTagsCall(args: SetAssetTagsMutationVariables): ActionTypeHelper<Promise<boolean>> {
    return WrapLoading(EDIT_ASSET_LOADING, async (dispatch, getState) => {
        const client = GetGraphQLClient()(dispatch, getState, undefined);
        const response = await client.mutate<SetAssetTagsMutation, SetAssetTagsMutationVariables>({
            mutation: SetAssetTags,
            variables: args,
        });

        if (response.errors) {
            throw new Error(response.errors.map((e) => e.message).join('\n'));
        }
        dispatch(EditAssetActionCreator());
        dispatch(enqueueSnackbar({ message: 'Successfully modified asset', options: { variant: 'success' } }));
        return true;
    });
}

export const DELETE_ASSET_LOADING = 'delete_asset_loading';

export function DeleteAssetCall(args: DeleteAssetMutationVariables): ActionTypeHelper<Promise<void>> {
    return WrapLoading(EDIT_ASSET_LOADING, async (dispatch, getState) => {
        const client = GetGraphQLClient()(dispatch, getState, undefined);
        const response = await client.mutate<DeleteAssetMutation, DeleteAssetMutationVariables>({
            mutation: DeleteAsset,
            variables: args,
        });

        if (response.errors) {
            throw new Error(response.errors.map((e) => e.message).join('\n'));
        }
        dispatch(DeleteAssetActionCreator());
        dispatch(enqueueSnackbar({ message: 'Successfully deleted asset', options: { variant: 'success' } }));
    });
}

export const UPSERT_ASSET_PERMISSION = 'upsert_asset_permission';

export function UpsertAssetPermissionCall(args: UpsertAssetPermissionMutationVariables): ActionTypeHelper<Promise<void>> {
    return WrapLoading(UPSERT_ASSET_PERMISSION, async (dispatch, getState) => {
        const client = GetGraphQLClient()(dispatch, getState, undefined);
        const response = await client.mutate<UpsertAssetPermissionMutation, UpsertAssetPermissionMutationVariables>({
            mutation: UpsertAssetPermission,
            variables: args,
        });

        if (response.errors) {
            throw new Error(response.errors.map((e) => e.message).join('\n'));
        }
        dispatch(UpsertAssetPermissionActionCreator());
        dispatch(enqueueSnackbar({ message: 'Successfully modified asset permissions', options: { variant: 'success' } }));
        // dispatch(FetchAsset({ id: args.assetId }));
    });
}

export const DELETE_ASSET_PERMISSION = 'delete_asset_permission';

export function DeleteAssetPermissionCall(args: DeleteAssetPermissionMutationVariables): ActionTypeHelper<Promise<void>> {
    return WrapLoading(DELETE_ASSET_PERMISSION, async (dispatch, getState) => {
        const client = GetGraphQLClient()(dispatch, getState, undefined);
        const response = await client.mutate<DeleteAssetPermissionMutation, DeleteAssetPermissionMutationVariables>({
            mutation: DeleteAssetPermission,
            variables: args,
        });

        if (response.errors) {
            throw new Error(response.errors.map((e) => e.message).join('\n'));
        }
        dispatch(DeleteAssetPermissionActionCreator());
        dispatch(enqueueSnackbar({ message: 'Successfully deleted asset permissions', options: { variant: 'success' } }));
    });
}
