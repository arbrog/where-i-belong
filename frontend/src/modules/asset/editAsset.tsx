import { makeStyles, Theme, Typography, CircularProgress, Fab, Tooltip, Box } from '@material-ui/core';
import React from 'react';
import { useSelector } from 'react-redux';
import { RootState } from '../../state/rootReducer';
import { ProvidePageTitle } from '../base/title';
import SaveIcon from '@material-ui/icons/Save';
import CloseIcon from '@material-ui/icons/Close';
import { useHistory, useParams } from 'react-router';
import { EditAssetCall, FetchAsset, FETCH_ASSET_LOADING_INDICATOR } from './state/actions';
import AddIcon from '@material-ui/icons/Add';
import { Field, Form } from 'react-final-form';
import { composeValidators, required, minLen } from '../../components/finalFormValidators';
import TextFieldWrapper from '../../components/textFieldFinalFormWrapper';
import arrayMutators from 'final-form-arrays';
import { FieldArray } from 'react-final-form-arrays';
import AssetMetdataFieldEditor from './components/assetMetdataFieldEditor';
import { AssetMetadataTypes } from '../../generated/graphql';
import { MetadataField, metadataFieldSubmitPrep, metadataFormIntialValue } from './util/metadataTypes';
import { FORM_ERROR } from 'final-form';
import { useStoreDispatch } from '../../state/store';
import SubmissionLoading from './components/editing/SubmissionLoading';
import { formStyles } from '../../components/formStyles';

const useStyles = makeStyles((theme: Theme) => ({
    backdrop: {
        zIndex: theme.zIndex.drawer + 1,
        color: '#fff',
    },
    container: {
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'column',
    },
    alignLeftCenter: {
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    root: {
        flexGrow: 1,
        padding: theme.spacing(2),
    },
    padCenter: { padding: theme.spacing(0.5), alignSelf: 'center' },
    titleContainer: { display: 'flex', flexDirection: 'row', alignSelf: 'flex-start' },
    mainTitle: { color: theme.palette.text.primary },
    secondaryTitle: { marginLeft: theme.spacing(1.5), color: theme.palette.text.disabled },
    loaderRoot: {
        display: 'flex',
        justifyContent: 'center',
        width: '100%',
    },
    progress: {
        margin: theme.spacing(2),
    },
    divider: { width: '100%', marginBottom: theme.spacing(5), marginTop: theme.spacing(3) },
    extendedIcon: {
        marginRight: theme.spacing(1),
    },
    fitContent: {
        height: 'fit-content',
    },
}));

const useFormStyles = makeStyles(formStyles);

const EditAsset: React.FunctionComponent = () => {
    const classes = useStyles({});
    const formClasses = useFormStyles({});
    const dispatch = useStoreDispatch();
    const { assetId } = useParams<{ assetId: string }>();
    const loading = useSelector((state: RootState) => state.loading.actions[FETCH_ASSET_LOADING_INDICATOR]?.loading);
    const { asset } = useSelector((state: RootState) => state.asset);
    const history = useHistory();
    const [submitting, setSubmitting] = React.useState(false);
    const [submissionStatus, setSubmissionStatus] = React.useState([] as string[]);

    const pushSubmissionStatus = React.useCallback((newStatus: string) => setSubmissionStatus((status) => [...status, newStatus]), []);

    React.useEffect(() => {
        dispatch(FetchAsset({ id: assetId }));
    }, [dispatch, assetId]);

    const submit = async (values: { name: string; metadata: { name: string; value: string; type: AssetMetadataTypes }[] }) => {
        setSubmitting(true);
        setSubmissionStatus(['Starting submission']);
        const keys = new Set();
        const errors = [] as string[];
        pushSubmissionStatus('Validating inputs');
        const parsedMetadata = values.metadata.map((metadata) => {
            if (keys.has(metadata.name)) {
                errors.push(`Duplicate key ${metadata.name}`);
            } else {
                keys.add(metadata.name);
            }
            return { name: metadata.name, data: metadata.value, type: metadata.type };
        });
        pushSubmissionStatus('Input validation complete');
        if (errors.length > 0) {
            setSubmitting(false);
            return { [FORM_ERROR]: errors };
        }

        //handle fieldLogic for each field
        const metdataPostSideEffects = await Promise.all(parsedMetadata.map(async (field) => await metadataFieldSubmitPrep(field, dispatch, pushSubmissionStatus)));

        pushSubmissionStatus('Presisting changes');
        await dispatch(
            EditAssetCall({
                id: assetId,
                asset: {
                    name: values.name,
                    metadata: metdataPostSideEffects,
                },
            }),
        );
        pushSubmissionStatus('Changes persisted');
        pushSubmissionStatus('Submission complete');
        setSubmitting(false);
        history.push(`/assets/asset/${assetId}`);
    };

    const initialValue = React.useMemo(() => {
        if (!asset) {
            return {};
        }
        return {
            name: asset.name,
            metadata: asset.metadata
                ? Object.entries(asset.metadata as Record<string, MetadataField>).map(([key, metadata]) => ({
                      name: key,
                      value: metadataFormIntialValue(metadata.type, metadata.data),
                      type: metadata.type,
                  }))
                : [],
        };
    }, [asset]);

    if (!asset || loading) {
        return (
            <div className={classes.container} style={{ alignItems: 'center' }}>
                <CircularProgress />
            </div>
        );
    }
    return (
        <div className={classes.container}>
            <ProvidePageTitle>Edit Asset</ProvidePageTitle>
            <SubmissionLoading open={submitting} onClose={() => null} label="Submitting Metadata Changes" state={submissionStatus} />
            <Form
                initialValues={initialValue}
                onSubmit={submit}
                mutators={{
                    ...arrayMutators,
                    setValue: ([field, value], state, { changeValue }) => {
                        changeValue(state, field, () => value);
                    },
                }}
                render={({
                    handleSubmit,
                    submitError,
                    form: {
                        mutators: { push, setValue },
                    },
                }) => (
                    <form onSubmit={handleSubmit}>
                        <div style={{ display: 'flex', justifyContent: 'space-between', width: '100%' }}>
                            <div style={{ alignSelf: 'flex-start' }}>
                                <div className={classes.titleContainer}>
                                    <Typography variant="h4" className={classes.mainTitle}>
                                        {asset.name}
                                    </Typography>
                                    <Typography variant="h6" className={classes.secondaryTitle} style={{ alignSelf: 'flex-end' }}>
                                        {`by ${asset.owner.firstName}`}
                                    </Typography>
                                </div>
                                <Typography className={classes.secondaryTitle} color="textSecondary" gutterBottom>
                                    Last Updated on {asset.updated_at}
                                </Typography>
                            </div>
                            {/* <pre>{JSON.stringify(values, null, 2)}</pre> */}
                            <div style={{ display: 'flex', alignItems: 'baseline' }}>
                                <Tooltip title="Save" arrow className={classes.extendedIcon}>
                                    <div className={classes.fitContent}>
                                        <Fab type="submit" aria-label="save" disabled={loading || submitting} color="primary">
                                            <SaveIcon />
                                        </Fab>
                                    </div>
                                </Tooltip>
                                <Tooltip title="Cancel" arrow className={classes.extendedIcon}>
                                    <div className={classes.fitContent}>
                                        <Fab aria-label="cancel" disabled={loading || submitting} color="secondary" onClick={() => history.push(`/assets/asset/${assetId}`)}>
                                            <CloseIcon />
                                        </Fab>
                                    </div>
                                </Tooltip>
                            </div>
                        </div>

                        <div style={{ alignSelf: 'flex-start' }}>
                            <div className={formClasses.formContainer}>
                                <div>
                                    <Field
                                        className={formClasses.formField}
                                        label="Asset Name"
                                        name="name"
                                        validate={composeValidators(required, minLen(1))}
                                        component={TextFieldWrapper}
                                    />
                                </div>
                                <div>
                                    <div className={classes.alignLeftCenter}>
                                        <Typography variant="h5" className={[classes.mainTitle, classes.extendedIcon].join(' ')}>
                                            Metadata
                                        </Typography>
                                        <Tooltip title="Add Value" aria-label="Add Value">
                                            <Fab size="small" color="primary" onClick={() => push('metadata', undefined)} style={{ alignSelf: 'center' }}>
                                                <AddIcon />
                                            </Fab>
                                        </Tooltip>
                                    </div>
                                    <FieldArray<{ name: string; value: string }> name="metadata">
                                        {({ fields }) =>
                                            fields.map((name, index) => (
                                                <Box key={name} p={0.25}>
                                                    <AssetMetdataFieldEditor
                                                        setValue={setValue}
                                                        field={{ name, index }}
                                                        removeSelf={() => fields.remove(index)}
                                                        value={fields.value?.[index]}
                                                    />
                                                </Box>
                                            ))
                                        }
                                    </FieldArray>
                                    {/* TODO: switch to snackbar with material ui 5 */}
                                    {submitError
                                        ? (submitError as string[]).map((error) => (
                                              <Typography variant="h6" color="error">
                                                  {error}
                                              </Typography>
                                          ))
                                        : null}
                                </div>
                            </div>
                        </div>
                    </form>
                )}
            />
        </div>
    );
};

export default EditAsset;
