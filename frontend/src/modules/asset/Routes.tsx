import React from 'react';
import { Route } from 'react-router-dom';
import { ensureAuthenticated } from '../auth/wrapper';

const Assets = React.lazy(() => import('./Assets'));

export const AssetRoutes = (): React.ReactNode[] => [<Route key="assets" path="/assets" component={ensureAuthenticated(Assets)} />];
