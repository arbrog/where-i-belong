import { makeStyles, Theme, Typography, Grid, Divider } from '@material-ui/core';
import React from 'react';
import { ProvidePageTitle } from '../base/title';
import { GetAllAssets } from './state/actions';
import { ReactFlowProvider } from 'react-flow-renderer';
import { useStoreDispatch } from '../../state/store';
import ReactFlowView from './components/reactFlowView';
import SearchAsset from './components/searchAsset';

const useStyles = makeStyles((theme: Theme) => ({
    loaderRoot: {
        display: 'flex',
        justifyContent: 'center',
        width: '100%',
    },
    mainTitle: { marginBottom: 12, color: theme.palette.text.secondary },
    subTitle: { color: theme.palette.text.secondary, fontStyle: 'italic' },
    container: {
        padding: theme.spacing(3),
    },
    progress: {
        margin: theme.spacing(2),
    },
    mainContent: {
        minWidth: theme.spacing(50),
    },
    divider: { width: '100%', marginBottom: theme.spacing(2), marginTop: theme.spacing(2) },
    flowActions: {
        position: 'absolute',
        zIndex: 5,
        top: '10px',
        left: '10px',
    },
    infoIcon: {
        position: 'absolute',
        fontSize: '15px',
        right: '2px',
        bottom: '2px',
        cursor: 'default',
    },
}));

const NodeView: React.FunctionComponent = () => {
    const classes = useStyles({});
    const dispatch = useStoreDispatch();
    const [assetSearch, setAssetSearch] = React.useState('');
    //TODO: implement
    const [onlyOwned, setOnlyOwned] = React.useState(true);

    React.useEffect(() => {
        dispatch(GetAllAssets({ only_owned: onlyOwned }));
    }, [dispatch, onlyOwned]);

    return (
        <div className={classes.container}>
            <ProvidePageTitle>Tree View</ProvidePageTitle>
            <Grid container spacing={3}>
                <Grid item xs={12}>
                    <Typography variant="h5" className={classes.mainTitle}>
                        Tree View
                    </Typography>
                    <Typography className={classes.subTitle}>A hierarchical visulization of assets. Backspace deletes selected edge or node</Typography>
                </Grid>
            </Grid>
            <Grid container spacing={1} className={classes.mainContent} alignItems="flex-start" direction="column">
                <Grid item style={{ minWidth: 'inherit' }}>
                    <SearchAsset setAssetName={setAssetSearch} onlyOwned={onlyOwned} setOnlyOwned={setOnlyOwned} />
                </Grid>
            </Grid>
            <Divider className={classes.divider} />
            <div style={{ height: '750px' }}>
                <ReactFlowProvider>
                    <ReactFlowView assetSearch={assetSearch} />
                </ReactFlowProvider>
            </div>
        </div>
    );
};

export default NodeView;
