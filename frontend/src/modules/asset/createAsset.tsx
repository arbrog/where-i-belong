import { CircularProgress, Grid, makeStyles, Theme, Typography } from '@material-ui/core';
import React from 'react';
import { grey } from '@material-ui/core/colors';
import { ProvidePageTitle } from '../base/title';
import { useSelector } from 'react-redux';
import { RootState } from '../../state/rootReducer';
import { useHistory, useLocation } from 'react-router';
import { AddAssetInput } from '../../generated/graphql';
import { CreateAssetCall, CREATE_ASSET_LOADING } from './state/actions';
import { useStoreDispatch } from '../../state/store';
import CreateAssetForm from './components/createAssetForm';
import { formStyles } from '../../components/formStyles';

const useStyles = makeStyles((theme: Theme) => ({
    mainTitle: { marginBottom: 12, color: theme.palette.text.secondary },
    root: {
        flexGrow: 1,
        padding: theme.spacing(2),
    },
    form: {
        padding: theme.spacing(4),
        marginTop: theme.spacing(6),
        border: `1px solid ${grey[400]}`,
        borderRadius: theme.spacing(0.75),
        backgroundColor: theme.palette.background.default,
    },
    submissionButtons: { paddingTop: theme.spacing(3), display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly' },
    divider: { width: '100%', marginBottom: theme.spacing(2), marginTop: theme.spacing(2) },
}));

const useFormStyles = makeStyles(formStyles);

const CreateAsset: React.FunctionComponent = () => {
    const classes = useStyles({});
    const formClasses = useFormStyles({});
    const dispatch = useStoreDispatch();
    const history = useHistory();

    const query = new URLSearchParams(useLocation().search);

    const initialFormValues: AddAssetInput = {
        name: '',
        parent: query.get('parent'),
        tags: [],
    };

    const loading = useSelector((state: RootState) => state.loading.actions[CREATE_ASSET_LOADING]?.loading);

    const submit = async (values: AddAssetInput, goBack = false) => {
        const asset = await dispatch(CreateAssetCall({ asset: values }));
        if (asset) {
            goBack ? history.goBack() : history.push(`/assets/asset/${asset.id}`);
        }
    };

    if (loading) {
        return (
            <div className={formClasses.container}>
                <div className={formClasses.formContainer}>
                    <CircularProgress />
                </div>
            </div>
        );
    }

    return (
        <>
            <ProvidePageTitle>Create Asset</ProvidePageTitle>
            <Grid container spacing={3}>
                <Grid item xs={12}>
                    <Typography variant="h5" className={classes.mainTitle}>
                        Create Asset
                    </Typography>
                </Grid>
            </Grid>
            <Grid container className={classes.root} spacing={2}>
                <Grid item xs={12}>
                    <Grid container justify="center" spacing={4}>
                        <CreateAssetForm submit={submit} initialFormValues={initialFormValues} formClasses={[formClasses.formContainer, formClasses.formContainerFull].join(' ')} />
                    </Grid>
                </Grid>
            </Grid>
        </>
    );
};

export default CreateAsset;
