import { makeStyles, Theme, Typography, Fab, Divider, Tooltip, Dialog, Button, DialogActions, DialogContent, DialogTitle } from '@material-ui/core';
import React from 'react';
import { useSelector } from 'react-redux';
import AddIcon from '@material-ui/icons/Add';
import dagre from 'dagre';
import ReactFlow, {
    Background,
    BackgroundVariant,
    Connection,
    Controls,
    Edge,
    Elements,
    isNode,
    MiniMap,
    Position,
    updateEdge,
    addEdge,
    removeElements,
    isEdge,
    OnLoadParams,
    useStoreState,
} from 'react-flow-renderer';
import { Form } from 'react-final-form';
import InfoIcon from '@material-ui/icons/Info';
import { useHistory } from 'react-router';
import { useStoreDispatch } from '../../../state/store';
import { RootState } from '../../../state/rootReducer';
import { CreateAssetCall, DeleteAssetCall, EditAssetCall, GetAllAssets } from '../state/actions';
import { AddAssetInput } from '../../../generated/graphql';
import EditAssetData from './editAssetData';
import { formStyles } from '../../../components/formStyles';

const useStyles = makeStyles((theme: Theme) => ({
    loaderRoot: {
        display: 'flex',
        justifyContent: 'center',
        width: '100%',
    },
    mainTitle: { marginBottom: 12, color: theme.palette.text.secondary },
    subTitle: { color: theme.palette.text.secondary, fontStyle: 'italic' },
    container: {
        padding: theme.spacing(3),
    },
    progress: {
        margin: theme.spacing(2),
    },
    mainContent: {
        minWidth: theme.spacing(50),
    },
    divider: { width: '100%', marginBottom: theme.spacing(2), marginTop: theme.spacing(2) },
    flowActions: {
        position: 'absolute',
        zIndex: 5,
        top: '10px',
        left: '10px',
    },
    infoIcon: {
        position: 'absolute',
        fontSize: '15px',
        right: '2px',
        bottom: '2px',
        cursor: 'default',
    },
}));

const useFormStyles = makeStyles(formStyles);

const ReactFlowView: React.FunctionComponent<{ assetSearch: string }> = ({ assetSearch }) => {
    const classes = useStyles({});
    const formClasses = useFormStyles({});
    const dispatch = useStoreDispatch();
    const history = useHistory();

    const [, setRfInstance] = React.useState<OnLoadParams<any> | null>(null);
    const nodes = useStoreState((state) => state.nodes);
    const { allAssets } = useSelector((state: RootState) => state.asset);
    const [addAsset, setAddAsset] = React.useState(false);
    const [isInit, setIsInit] = React.useState(true);
    const [awaitingLayout, setAwaitingLayout] = React.useState(true);
    const [prelayoutedElements, setPreLayoutedElements] = React.useState([] as Elements<any>);
    const [layoutedElements, setLayoutedElements] = React.useState([] as Elements<any>);

    const edgeExtras = {
        type: 'smoothstep',
        style: { strokeWidth: '0.25rem' },
    };

    React.useEffect(() => {
        if (!allAssets) {
            return;
        }
        const assetElements: Elements<any> = [];

        // create initial nodes and edges
        allAssets?.forEach((asset) => {
            //add node
            assetElements.push({
                id: asset.id,
                type: 'default',
                data: {
                    label: (
                        <>
                            <Typography variant="subtitle2">{asset.name}</Typography>
                            <InfoIcon className={classes.infoIcon} color="action" onClick={() => history.push(`/assets/asset/${asset.id}`)} />
                        </>
                    ),
                    isRoot: asset.parent?.id ? false : true,
                },
                position: { x: 0, y: 0 },
            });
            //add edge
            if (asset.parent?.id) {
                assetElements.push({
                    id: `${asset.id}-${asset.parent?.id}`,
                    source: asset.parent?.id,
                    target: asset.id,
                    ...edgeExtras,
                });
            }
        });

        setPreLayoutedElements(assetElements);
        if (isInit) {
            setIsInit(false);
            setLayoutedElements(assetElements);
        }
        setAwaitingLayout(true);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [allAssets]);

    React.useEffect(() => {
        // don't format until nodes have been initialized
        if (prelayoutedElements.length === 0 || nodes.length === 0 || !awaitingLayout) {
            return;
        }

        //if nodes actually have size
        if (!nodes[0].__rf.height) {
            return;
        }

        const dagreGraph = new dagre.graphlib.Graph();
        dagreGraph.setDefaultEdgeLabel(() => ({}));

        //use dagreGraph to layout elements
        const getLayoutedElements = (assetElements: Elements<any>, direction: 'TB' | 'LR' = 'TB') => {
            const isHorizontal = direction === 'LR';
            dagreGraph.setGraph({ rankdir: direction });

            const nodeWidthDefault = 150;
            const nodeHeightDefault = 10;

            assetElements.forEach((el) => {
                if (isNode(el)) {
                    const node = nodes.find((node) => node.id === el.id);
                    dagreGraph.setNode(el.id, { width: node?.__rf.width || nodeWidthDefault, height: node?.__rf.height || nodeHeightDefault });
                } else {
                    dagreGraph.setEdge(el.source, el.target);
                }
            });

            dagre.layout(dagreGraph);

            return assetElements.map((el) => {
                if (isNode(el)) {
                    const nodeWithPosition = dagreGraph.node(el.id);
                    el.targetPosition = isHorizontal ? Position.Left : Position.Top;
                    el.sourcePosition = isHorizontal ? Position.Right : Position.Bottom;

                    // unfortunately we need this little hack to pass a slighltiy different position
                    // to notify react flow about the change. More over we are shifting the dagre node position
                    // (anchor=center center) to the top left so it matches the react flow node anchor point (top left).
                    el.position = {
                        x: nodeWithPosition.x - nodeWithPosition.width / 2 + Math.random() / 1000,
                        y: nodeWithPosition.y - nodeWithPosition.height / 2,
                    };
                }

                return el;
            });
        };

        setLayoutedElements(getLayoutedElements(prelayoutedElements));
        setAwaitingLayout(false);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [prelayoutedElements, nodes, awaitingLayout]);

    const hideNodes = React.useCallback(
        (assetSearch: string) => (elements: Elements): Elements => {
            const hidden = new Set();
            const modified = elements.map((element) => {
                if (isNode(element)) {
                    const asset = allAssets?.find((asset) => asset.id == element.id);
                    if ((asset?.name || '').toLocaleLowerCase().indexOf(assetSearch.toLowerCase()) === -1) {
                        element.isHidden = true;
                        hidden.add(element.id);
                    } else {
                        element.isHidden = false;
                    }
                }
                return element;
            });
            modified.filter(isEdge).forEach((element) => {
                element.isHidden = hidden.has(element.source) || hidden.has(element.target);
            });
            return modified;
        },
        [allAssets],
    );

    // eslint-disable-next-line react-hooks/exhaustive-deps
    React.useEffect(() => setLayoutedElements(hideNodes(assetSearch)), [allAssets, assetSearch]);

    // connect new edge to node
    const onConnect = async (params: Edge<any> | Connection) => {
        if (params.source && params.target) {
            const success = await dispatch(EditAssetCall({ id: params.target, asset: { parent: params.source } }));
            if (success) {
                const edgeToRemove: Elements<any> = layoutedElements.filter((elem) => isEdge(elem) && elem.target === params.target);
                return setLayoutedElements((els: Elements<any>) => addEdge({ ...params, ...edgeExtras }, removeElements(edgeToRemove, els)));
            }
        }
        return layoutedElements;
    };

    // deleting node or edge //TODO: handle failure
    const onElementsRemove = async (elementsToRemove: Elements<any>) => {
        const removeBatch = elementsToRemove.map(async (elementToRemove) => {
            if (isNode(elementToRemove)) {
                return dispatch(DeleteAssetCall({ id: elementToRemove.id }));
            } else {
                return dispatch(EditAssetCall({ id: elementToRemove.target, asset: { parent: null } }));
            }
        });
        await Promise.all(removeBatch);
        setLayoutedElements((els: Elements<any>) => removeElements(elementsToRemove, els));
    };

    // update existing edge //TODO: improve failure
    const onEdgeUpdate = async (oldEdge: Edge<any>, newConnection: Connection) => {
        if (newConnection.source && newConnection.target) {
            //clear old parent
            const success = await dispatch(EditAssetCall({ id: oldEdge.target, asset: { parent: null } }));
            //add new parent
            const successNew = await dispatch(EditAssetCall({ id: newConnection.target, asset: { parent: newConnection.source } }));
            if (!success || !successNew) {
                return;
            }
        }
        setLayoutedElements((els: Elements<any>) => updateEdge(oldEdge, { ...newConnection, ...edgeExtras }, els));
    };

    //create new node
    const addNode = async (values: AddAssetInput) => {
        const asset = await dispatch(CreateAssetCall({ asset: values }));
        if (asset) {
            setLayoutedElements([
                ...layoutedElements,
                {
                    id: asset.id,
                    type: 'default',
                    data: { label: <Typography variant="subtitle2">{asset.name}</Typography>, isRoot: asset.parent?.id ? false : true },
                    position: { x: 0, y: 0 },
                },
            ]);
            dispatch(GetAllAssets({ only_owned: true }));
        }
        setAddAsset(false);
        setAwaitingLayout(true);
    };

    return (
        <ReactFlow
            minZoom={0.01}
            onlyRenderVisibleElements={false}
            elements={layoutedElements}
            snapToGrid={true}
            onConnect={onConnect}
            onLoad={setRfInstance}
            onElementsRemove={onElementsRemove}
            onEdgeUpdate={onEdgeUpdate}
        >
            <div className={classes.flowActions}>
                <Tooltip title="Add Asset" aria-label="Add Asset">
                    <Fab size="small" color="primary" onClick={() => setAddAsset(true)} style={{ alignSelf: 'center' }}>
                        <AddIcon />
                    </Fab>
                </Tooltip>
                <Dialog
                    open={addAsset}
                    onClose={() => setAddAsset(false)}
                    aria-labelledby="form-dialog-title"
                    // maxWidth={'sm'}
                    // fullWidth={true}
                    PaperProps={{ style: { overflow: 'visible' } }}
                >
                    <Form
                        initialValues={{ name: '', tags: [] }}
                        onSubmit={addNode}
                        render={({ handleSubmit }) => (
                            <form className={formClasses.formContainer} onSubmit={handleSubmit}>
                                <DialogTitle id="form-dialog-title">Add Asset</DialogTitle>
                                <DialogContent style={{ overflow: 'visible' }}>
                                    <EditAssetData />
                                    <Divider className={classes.divider} />
                                </DialogContent>

                                <DialogActions>
                                    <Tooltip title="Cancel" aria-label="Cancel">
                                        <Button onClick={() => setAddAsset(false)} color="secondary">
                                            Cancel
                                        </Button>
                                    </Tooltip>
                                    <Tooltip title="Submit" aria-label="Submit">
                                        <Button type="submit" color="primary">
                                            Submit
                                        </Button>
                                    </Tooltip>
                                </DialogActions>
                            </form>
                        )}
                    />
                </Dialog>
            </div>
            <MiniMap
                nodeStrokeColor={(n) => {
                    if (n.style?.background) return n.style.background as string;
                    if (n.data.isRoot) return '#0041d0';
                    if (n.type === 'default') return '#1a192b';

                    return '#eee';
                }}
                nodeColor={(n) => {
                    if (n.style?.background) return n.style.background as string;
                    //FIXME: not working
                    if (n.data.isRoot) return '#0041d0';

                    return '#fff';
                }}
                nodeBorderRadius={2}
            />
            <Controls />
            <Background variant={BackgroundVariant.Dots} gap={15} size={2} />
        </ReactFlow>
    );
};

export default ReactFlowView;
