import { makeStyles, Theme, Typography, Card, CardContent, Grid, CardActions, Button } from '@material-ui/core';
import React from 'react';
import { Link } from 'react-router-dom';
import { DateTime } from 'luxon';
import { IAssetNoChild } from '../state/reducer';

const useStyles = makeStyles((theme: Theme) => ({
    cardGridRoot: {
        [theme.breakpoints.down('sm')]: {
            width: '70%',
        },
        [theme.breakpoints.up('md')]: {
            width: '40%',
        },
        [theme.breakpoints.up('lg')]: {
            width: '30%',
            maxWidth: '35rem',
        },
    },
    cardRoot: {
        minWidth: '15rem',
        background: theme.palette.background.default,
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
}));

const AssetCard: React.FunctionComponent<{ asset: IAssetNoChild }> = ({ asset }) => {
    const classes = useStyles({});

    return (
        <Grid className={classes.cardGridRoot} item>
            <Card className={classes.cardRoot}>
                <CardContent>
                    <Typography variant="h5" component="h2">
                        {asset.name}
                    </Typography>
                    <Typography className={classes.pos} color="textSecondary">
                        {`${asset.owner.firstName}`}
                    </Typography>
                    <Typography className={classes.title} color="textSecondary" gutterBottom>
                        Created On {DateTime.fromISO(asset.created_at).toLocaleString(DateTime.DATETIME_MED)}
                    </Typography>
                    <Typography className={classes.title} color="textSecondary" gutterBottom>
                        Last Updated {DateTime.fromISO(asset.updated_at).toLocaleString(DateTime.DATETIME_MED)}
                    </Typography>
                </CardContent>
                <CardActions>
                    <Link to={`/assets/asset/${asset.id}`} key={asset.id} style={{ textDecoration: 'none' }}>
                        <Button size="small">View Asset</Button>
                    </Link>
                </CardActions>
            </Card>
        </Grid>
    );
};

export default AssetCard;
