import { Button, Avatar, ListItem, ListItemAvatar, ListItemText, Tooltip, makeStyles, Theme } from '@material-ui/core';
import React from 'react';
import { AssetPermissionType, GetAssetQuery } from '../../../generated/graphql';
import gravatar from 'gravatar';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import VisibilityIcon from '@material-ui/icons/Visibility';
import RemoveCircleIcon from '@material-ui/icons/RemoveCircle';
import { useStoreDispatch } from '../../../state/store';
import { DeleteAssetPermissionCall, FetchAsset, UpsertAssetPermissionCall } from '../state/actions';

const withStyles = makeStyles((_theme: Theme) => ({
    buttonGroup: {
        '&>:first-child': {
            borderTopRightRadius: 0,
            borderBottomRightRadius: 0,
        },
        '&>:last-child': {
            borderTopLeftRadius: 0,
            borderBottomLeftRadius: 0,
        },
        '&>:not(:first-child):not(:last-child)': {
            borderRadius: 0,
            borderRight: 0,
            borderLeft: 0,
        },
        // "width": 200,
    },
}));

const AssetPermissionSelector: React.FunctionComponent<{ permission: GetAssetQuery['getAsset']['permissions'][0]; assetId: string; disabled: boolean }> = ({
    permission,
    assetId,
    disabled = false,
}) => {
    const classes = withStyles({});
    const dispatch = useStoreDispatch();
    const [permissions, setPermissions] = React.useState<AssetPermissionType[]>([]);

    //sync permissions input to local state
    React.useEffect(() => {
        setPermissions([...permission.permissions]);
    }, [permission]);

    const togglePermission = (togglePermission: AssetPermissionType) => (currentValue: AssetPermissionType[]) => {
        let newPerms = [];
        if (currentValue.includes(togglePermission)) {
            newPerms = currentValue.filter((perm) => perm !== togglePermission);
        } else {
            newPerms = [...currentValue, togglePermission];
        }
        // TODO: if an error occurs then perms will be out of sync
        dispatch(UpsertAssetPermissionCall({ assetId, targetUser: permission.target.id, permissions: newPerms }));
        return newPerms;
    };

    const hasViewPermission = React.useMemo(() => permissions.includes(AssetPermissionType.View), [permissions]);
    const hasEditPermission = React.useMemo(() => permissions.includes(AssetPermissionType.Edit), [permissions]);
    const hasDeletePermission = React.useMemo(() => permissions.includes(AssetPermissionType.Delete), [permissions]);

    const TooltipExt = (props: any) => {
        return !disabled ? <Tooltip {...props}>{props.children}</Tooltip> : <>{props.children}</>;
    };

    return (
        <ListItem key={permission.target.email}>
            <ListItemAvatar>
                <Avatar alt={permission.target.email} src={gravatar.url(permission.target.email, { protocol: 'https', r: 'pg' })} />
            </ListItemAvatar>
            <ListItemText primary={permission.target.email} secondary={`${permission.target.firstName} ${permission.target.lastName}`} />

            <div className={classes.buttonGroup} aria-label="permission button group" style={{ paddingLeft: '1rem' }}>
                <TooltipExt title={`${hasViewPermission ? 'Remove' : 'Add'} View Permission`}>
                    <Button
                        size="small"
                        color="primary"
                        disabled={disabled}
                        variant={hasViewPermission ? 'contained' : 'outlined'}
                        aria-label="view"
                        onClick={() => {
                            setPermissions(togglePermission(AssetPermissionType.View));
                        }}
                    >
                        <VisibilityIcon />
                    </Button>
                </TooltipExt>
                <TooltipExt title={`${hasEditPermission ? 'Remove' : 'Add'} Edit Permission`}>
                    <Button
                        size="small"
                        color="primary"
                        disabled={disabled}
                        variant={hasEditPermission ? 'contained' : 'outlined'}
                        aria-label="edit"
                        onClick={() => {
                            setPermissions(togglePermission(AssetPermissionType.Edit));
                        }}
                    >
                        <EditIcon />
                    </Button>
                </TooltipExt>
                <TooltipExt title={`${hasDeletePermission ? 'Remove' : 'Add'} Delete Permission`}>
                    <Button
                        size="small"
                        color="primary"
                        disabled={disabled}
                        variant={hasDeletePermission ? 'contained' : 'outlined'}
                        aria-label="delete"
                        onClick={() => {
                            setPermissions(togglePermission(AssetPermissionType.Delete));
                        }}
                    >
                        <RemoveCircleIcon />
                    </Button>
                </TooltipExt>
            </div>
            <Tooltip title="Delete Entry">
                <span>
                    <Button
                        disabled={disabled}
                        size="small"
                        aria-label="remove"
                        color="secondary"
                        style={{ marginLeft: '0.25rem' }}
                        onClick={async () => {
                            await dispatch(DeleteAssetPermissionCall({ permId: permission.id }));
                            dispatch(FetchAsset({ id: assetId }));
                        }}
                    >
                        <DeleteIcon />
                    </Button>
                </span>
            </Tooltip>
        </ListItem>
    );
};

export default AssetPermissionSelector;
