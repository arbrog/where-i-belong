import { Dialog, Button, DialogActions, DialogContent, DialogTitle, CircularProgress } from '@material-ui/core';
import QRCode from 'qrcode';
import React from 'react';
import { IAssetNoChild } from '../state/reducer';

const ShowQRCode: React.FunctionComponent<{
    open: boolean;
    handleClose: () => void;
    asset: IAssetNoChild;
}> = ({ open, handleClose, asset }) => {
    const isCancelled = React.useRef(false);
    const [imgSource, setImgSource] = React.useState('');

    React.useEffect(() => {
        const asyncFunction = async () => {
            const imgSource = await QRCode.toString(asset.id, { type: 'svg', errorCorrectionLevel: 'M' });
            if (!isCancelled.current) {
                setImgSource(imgSource);
            }
        };
        asyncFunction();
        return () => {
            isCancelled.current = true;
        };
    }, [asset.id]);
    const qrSVG = `data:image/svg+xml;base64,${btoa(imgSource)}`;

    return (
        <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title" maxWidth={'sm'} fullWidth={true} PaperProps={{ style: { overflow: 'visible' } }}>
            <DialogTitle id="form-dialog-title">QR Code</DialogTitle>
            <DialogContent style={{ overflow: 'visible' }}>
                <div style={{ display: 'flex' }}>{imgSource ? <img src={qrSVG} style={{ height: '100%', width: '100%' }} /> : <CircularProgress />}</div>
            </DialogContent>
            <DialogActions>
                <a style={{ textDecoration: 'none' }} href={qrSVG} download={`${asset.name.replace(/\s+/g, '')}.svg`}>
                    <Button onClick={handleClose} color="primary" variant="outlined">
                        Download QR Code
                    </Button>
                </a>
                <Button onClick={handleClose} color="secondary" variant="outlined">
                    Close
                </Button>
            </DialogActions>
        </Dialog>
    );
};

export default ShowQRCode;
