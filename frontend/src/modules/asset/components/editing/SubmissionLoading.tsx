import React from 'react';
import { Dialog, DialogTitle, Box, LinearProgress, Typography, Paper } from '@material-ui/core';

const SubmissionLoading: React.FunctionComponent<{ open: boolean; onClose: () => void; label: string; state: string[] }> = ({ open, onClose, label, state }) => {
    return (
        <Dialog onClose={onClose} aria-labelledby="submitting" open={open}>
            <DialogTitle id="submitting">{label}</DialogTitle>
            <Box p={2}>
                <LinearProgress style={{ marginBottom: '1rem' }} />
                <Paper>
                    <Box p={1} style={{ display: 'flex', flexDirection: 'column' }}>
                        {state.map((message) => (
                            <Typography key={message} variant="overline">
                                {message}
                            </Typography>
                        ))}
                    </Box>
                </Paper>
            </Box>
        </Dialog>
    );
};

export default SubmissionLoading;
