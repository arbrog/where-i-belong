import { Dialog, DialogTitle, DialogContent, DialogActions, Button, makeStyles } from '@material-ui/core';
import React from 'react';
import { Field, Form } from 'react-final-form';
import { composeValidators, isEmail, required } from '../../../components/finalFormValidators';
import { FetchAsset, UpsertAssetPermissionCall } from '../state/actions';
import { IAsset } from '../state/reducer';
import TextFieldWrapper from '../../../components/textFieldFinalFormWrapper';
import { GetUserCall } from '../../profile/state/actions';
import { useStoreDispatch } from '../../../state/store';
import { formStyles } from '../../../components/formStyles';

const useFormStyles = makeStyles(formStyles);

const SelectPermissionTarget: React.FunctionComponent<{
    open: boolean;
    asset: IAsset;
    handleClose: () => void;
}> = ({ open, asset, handleClose }) => {
    const formClasses = useFormStyles({});
    const dispatch = useStoreDispatch();

    const submit = async ({ email }: { email: string }) => {
        const user = await dispatch(GetUserCall({ email }));
        if (!user) {
            return { email: 'Email is Incorrect' };
        } else if (user.id === asset.owner.id) {
            return { email: 'Can not edit permissions of asset owner' };
        } else if (asset?.permissions?.some((perm) => perm.target.id === user.id)) {
            return { email: 'User already has permissions to this asset' };
        }
        await dispatch(UpsertAssetPermissionCall({ assetId: asset.id, targetUser: user.id, permissions: [] }));
        dispatch(FetchAsset({ id: asset.id }));
        handleClose();
    };

    return (
        <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title" maxWidth={'sm'} fullWidth={true} PaperProps={{ style: { overflow: 'visible' } }}>
            <Form
                initialValues={{ email: '' }}
                onSubmit={submit}
                render={({ handleSubmit }) => (
                    <form className={formClasses.formContainer} onSubmit={handleSubmit}>
                        <DialogTitle id="form-dialog-title">Share Asset with User</DialogTitle>
                        <DialogContent style={{ overflow: 'visible' }}>
                            <div className={formClasses.formContainer} style={{}}>
                                <Field className={formClasses.formField} label="Email" name="email" validate={composeValidators(required, isEmail)} component={TextFieldWrapper} />
                            </div>
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={handleClose} color="secondary">
                                Cancel
                            </Button>
                            <Button type="submit" color="primary">
                                Share
                            </Button>
                        </DialogActions>
                    </form>
                )}
            />
        </Dialog>
    );
};

export default SelectPermissionTarget;
