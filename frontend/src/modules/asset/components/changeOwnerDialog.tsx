import { Dialog, DialogTitle, DialogContent, DialogActions, Button, makeStyles } from '@material-ui/core';
import React from 'react';
import { Field, Form } from 'react-final-form';
import { useDispatch } from 'react-redux';
import { ModifyAssetInput } from '../../../generated/graphql';
import { composeValidators } from '../../../components/finalFormValidators';
import SelectAsset from './selectAsset';
import { EditAssetCall, FetchAsset } from '../state/actions';
import { formStyles } from '../../../components/formStyles';

const useFormStyles = makeStyles(formStyles);

const ChangeOwnerDialog: React.FunctionComponent<{
    open: boolean;
    change: boolean;
    handleClose: () => void;
    initialValues: ModifyAssetInput;
    id: string;
}> = ({ open, change, handleClose, initialValues, id }) => {
    const formClasses = useFormStyles({});
    const dispatch = useDispatch();

    const submit = async (values: ModifyAssetInput) => {
        await dispatch(EditAssetCall({ id, asset: values }));
        dispatch(FetchAsset({ id }));
        handleClose();
    };

    return (
        <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title" maxWidth={'sm'} fullWidth={true} PaperProps={{ style: { overflow: 'visible' } }}>
            <Form
                initialValues={initialValues}
                onSubmit={submit}
                render={({ handleSubmit, dirty }) => (
                    <form onSubmit={handleSubmit}>
                        <DialogTitle id="form-dialog-title">{change ? 'Edit Parent' : 'Set Parent'}</DialogTitle>
                        <DialogContent style={{ overflow: 'visible' }}>
                            <div className={formClasses.formContainer} style={{}}>
                                <Field name="parent" label="Parent" validate={composeValidators()}>
                                    {({ input: { value, onChange }, label, meta }) => <SelectAsset value={value} onChange={onChange} label={label} meta={meta} />}
                                </Field>
                            </div>
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={handleClose} color="secondary">
                                Cancel
                            </Button>
                            <Button type="submit" color="primary" disabled={!dirty}>
                                {change ? 'Set' : 'Edit'}
                            </Button>
                        </DialogActions>
                    </form>
                )}
            />
        </Dialog>
    );
};

export default ChangeOwnerDialog;
