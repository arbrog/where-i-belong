import { FormHelperText, Tooltip, makeStyles, Theme, Button, Checkbox, FormControlLabel } from '@material-ui/core';
import React from 'react';
import { useSelector } from 'react-redux';
import Select, { ValueType } from 'react-select';
import { RootState } from '../../../state/rootReducer';
import { useStoreDispatch } from '../../../state/store';
import { GetAssetHelper, SearchForAssets, SEARCH_ASSETS_LOADING_INDICATOR } from '../state/actions';
import { IAsset } from '../state/reducer';
import CameraAltIcon from '@material-ui/icons/CameraAlt';
import CloseIcon from '@material-ui/icons/Close';
import FlipCameraIosIcon from '@material-ui/icons/FlipCameraIos';
import QrReader from 'react-qr-reader';
import { formStyles } from '../../../components/formStyles';

type selectType = {
    value: string;
    label: string;
};

enum CameraFacing {
    user = 'user',
    environment = 'environment',
}

const useStyles = makeStyles((theme: Theme) => ({
    extendedIcon: {
        marginRight: theme.spacing(1),
    },
    fitContent: {
        height: 'fit-content',
        width: 'fit-content',
    },
    iconDiv: {
        display: 'flex',
        paddingBottom: theme.spacing(0.5),
    },
}));

const useFormStyles = makeStyles(formStyles);

const SelectAsset: React.FunctionComponent<{
    value: string | string[];
    onChange: (event: any) => void;
    label: React.ReactNode;
    meta?: any;
}> = ({ value, onChange, label, meta }) => {
    const classes = useStyles({});
    const dispatch = useStoreDispatch();
    const formClasses = useFormStyles({});
    const [onlyOwned, setOnlyOwned] = React.useState(false);
    const [showQRCode, setShowQRCode] = React.useState(false);
    const [cameraFacing, setCameraFacing] = React.useState(CameraFacing.environment);
    const [qrError, setQRError] = React.useState('');
    const showError = meta ? ((meta.submitError && !meta.dirtySinceLastSubmit) || meta.error) && meta.touched : qrError ? true : false;

    const assets = useSelector((state: RootState) => state.asset.searchAssets) || new Array<IAsset>();
    const assetsLoading = useSelector((state: RootState) => state.loading.actions[SEARCH_ASSETS_LOADING_INDICATOR]?.loading);

    React.useEffect(() => {
        dispatch(SearchForAssets({ name: '', only_owned: onlyOwned }));
    }, [dispatch, onlyOwned]);

    const internalOptions: selectType[] = assets.map((asset) => ({ value: asset.id, label: asset.name }));

    let assetValue: selectType[] | selectType | null = null;
    if (value) {
        assetValue = internalOptions?.find((option) => option.value === value) || null;
    }

    const selectProps = {
        value: assetValue,
        isLoading: assetsLoading,
        isClearable: true,
        options: internalOptions,
        onChange: (options: ValueType<selectType, false>) => {
            if (options) {
                onChange(assets?.find((asset) => asset.id === (options as selectType).value)?.id);
            } else {
                onChange(null);
            }
        },
        styles: {
            menu: (provided: any) => ({
                ...provided,
                zIndex: 10,
            }),
        },
    };

    const handleScan = async (data: string | null) => {
        if (data) {
            const asset = await dispatch(GetAssetHelper({ id: data }));
            if (asset) {
                onChange(asset.id);
                setShowQRCode(false);
            } else {
                onChange(undefined);
            }
        }
    };

    return (
        <div className={formClasses.formField}>
            {label}
            <Select {...selectProps} />
            <FormControlLabel
                control={<Checkbox checked={onlyOwned} onChange={(_) => setOnlyOwned((value) => !value)} name="only_owned" color="primary" />}
                label="Show Only Owned Assets"
            />
            <div style={{ marginTop: '1rem' }}>
                {showQRCode ? (
                    <>
                        <div className={classes.iconDiv}>
                            <Tooltip title="Close Camera" arrow className={classes.extendedIcon}>
                                <div className={classes.fitContent}>
                                    <Button variant="outlined" color="secondary" onClick={() => setShowQRCode(false)}>
                                        <CloseIcon />
                                    </Button>
                                </div>
                            </Tooltip>
                            <Tooltip title="Swap Camera" arrow className={classes.extendedIcon}>
                                <div className={classes.fitContent}>
                                    <Button
                                        variant="outlined"
                                        color="primary"
                                        onClick={() => setCameraFacing((value) => (value === CameraFacing.environment ? CameraFacing.user : CameraFacing.environment))}
                                    >
                                        <FlipCameraIosIcon />
                                    </Button>
                                </div>
                            </Tooltip>
                        </div>
                        <QrReader resolution={720} facingMode={cameraFacing} onError={(err: any) => setQRError(err)} onScan={handleScan} style={{ width: '100%' }} />
                    </>
                ) : (
                    <Tooltip title="Scan QR Code" arrow className={classes.extendedIcon}>
                        <div className={classes.fitContent}>
                            <Button variant="outlined" color="primary" onClick={() => setShowQRCode(true)}>
                                <CameraAltIcon />
                            </Button>
                        </div>
                    </Tooltip>
                )}
            </div>

            {showError ? <FormHelperText error={showError}>{meta.error || meta.submitError || qrError}</FormHelperText> : null}
        </div>
    );
};

export default SelectAsset;
