import { makeStyles, Grid, TextField, FormControlLabel, Checkbox, Theme } from '@material-ui/core';
import React from 'react';
import SearchIcon from '@material-ui/icons/Search';

const useStyles = makeStyles((theme: Theme) => ({
    search: {
        minWidth: theme.spacing(25),
    },
}));

const SearchAsset: React.FunctionComponent<{
    setAssetName: React.Dispatch<React.SetStateAction<string>>;
    onlyOwned: boolean;
    setOnlyOwned: React.Dispatch<React.SetStateAction<boolean>>;
}> = ({ setAssetName, onlyOwned, setOnlyOwned }) => {
    const classes = useStyles({});

    return (
        <Grid container spacing={1} alignItems="flex-end">
            <Grid item>
                <SearchIcon />
            </Grid>
            <Grid item>
                <TextField label="Search Asset Name" onChange={(event) => setAssetName(event.target.value)} className={classes.search} />
            </Grid>
            <Grid item>
                <FormControlLabel
                    control={<Checkbox checked={onlyOwned} onChange={(_) => setOnlyOwned((value) => !value)} name="only_owned" color="primary" />}
                    label="Show Only Owned Assets"
                />
            </Grid>
        </Grid>
    );
};

export default SearchAsset;
