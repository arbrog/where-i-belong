import { useStoreDispatch } from '../../../state/store';
import { useSelector } from 'react-redux';
import { RootState } from '../../../state/rootReducer';
import { GetImageURIAction } from '../../storage/state/actions';
import * as React from 'react';
import { Button, CircularProgress, Dialog, DialogActions, DialogContent, DialogTitle, Link } from '@material-ui/core';
import { MetadataField } from '../util/metadataTypes';
import { getCachedImageURI } from '../../storage/state/reducer';

const StorageImageRenderer: React.FunctionComponent<{ field: MetadataField; fieldKey: string }> = ({ field, fieldKey }) => {
    const dispatch = useStoreDispatch();
    const uri = useSelector((state: RootState) => getCachedImageURI(field.data, () => state)?.url);
    const [open, setOpen] = React.useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    React.useEffect(() => {
        dispatch(GetImageURIAction({ name: field.data }));
    }, [dispatch, field]);

    if (!uri) {
        return <CircularProgress />;
    }
    return (
        <>
            <Button variant="outlined" color="primary" onClick={handleClickOpen}>
                View Image
            </Button>
            <Dialog maxWidth="xl" open={open} onClose={handleClose} aria-labelledby="image-title">
                <DialogTitle id="image-title">
                    <Link href={uri} target="_blank">
                        {fieldKey}
                    </Link>
                </DialogTitle>
                <DialogContent>
                    <img src={uri} style={{ maxHeight: '100%', width: '100%' }} />
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        Close
                    </Button>
                </DialogActions>
            </Dialog>
        </>
    );
};

export default StorageImageRenderer;
