import * as React from 'react';
import { Field } from 'react-final-form';
import DeleteIcon from '@material-ui/icons/Delete';
import { Tooltip, IconButton, makeStyles, Theme, FormControl, InputLabel, MenuItem, Select, Paper, FormHelperText } from '@material-ui/core';
import { composeValidators, isNumber, required } from '../../../components/finalFormValidators';
import TextFieldWrapper from '../../../components/textFieldFinalFormWrapper';
import { AssetMetadataTypes } from '../../../generated/graphql';
import DateTimeWrapper from '../../../components/DateTimeFinalFormWrapper';
import CheckboxFieldWrapper from '../../../components/CheckboxFinalFormWrapper';
import ImageUploaderFinalFormWrapper, { customImageRequired } from '../../storage/components/ImageUploaderFinalFormWrapper';

const useStyles = makeStyles((theme: Theme) => ({
    fieldGroupcontainer: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingTop: theme.spacing(1),
    },
    formSection: {
        padding: theme.spacing(3, 1, 3, 1),
        display: 'flex',
        alignItems: 'center',
    },
    leftRightPadding: {
        margin: theme.spacing(0, 1, 0, 1),
    },
}));

const AssetMetdataFieldEditor: React.FunctionComponent<{
    field: { name: string; index: number };
    setValue: (path: string, value: any) => void;
    removeSelf: () => void;
    value: any;
}> = ({ field, removeSelf, value, setValue }) => {
    const classes = useStyles({});

    const defaultFieldProps = { label: 'Value', name: `${field.name}.value`, className: classes.leftRightPadding };
    let valueSelector = null;

    if (value?.type === AssetMetadataTypes.String) {
        valueSelector = <Field {...defaultFieldProps} validate={composeValidators(required)} component={TextFieldWrapper} />;
    } else if (value?.type === AssetMetadataTypes.Number) {
        valueSelector = <Field {...defaultFieldProps} validate={composeValidators(required, isNumber)} component={TextFieldWrapper} />;
    } else if (value?.type === AssetMetadataTypes.Date) {
        valueSelector = <Field {...defaultFieldProps} validate={composeValidators(required)} component={DateTimeWrapper} />;
    } else if (value?.type === AssetMetadataTypes.Boolean) {
        // initial value doesn't seem to work
        valueSelector = <Field {...defaultFieldProps} validate={composeValidators(required)} type="checkbox" initialValue={false} component={CheckboxFieldWrapper} />;
    } else if (value?.type === AssetMetadataTypes.Image) {
        valueSelector = <Field {...defaultFieldProps} validate={composeValidators(required, customImageRequired)} component={ImageUploaderFinalFormWrapper} />;
    }

    return (
        <Paper>
            <div className={classes.fieldGroupcontainer}>
                <div className={classes.formSection}>
                    <Field
                        className={classes.leftRightPadding}
                        label="Field Name"
                        name={`${field.name}.name`}
                        validate={composeValidators(required)}
                        component={TextFieldWrapper}
                    />
                    <Field
                        className={classes.leftRightPadding}
                        label="Field Type"
                        name={`${field.name}.type`}
                        validate={composeValidators(required)}
                        render={({ input: { value, onChange }, meta }) => {
                            const showError = meta ? ((meta.submitError && !meta.dirtySinceLastSubmit) || meta.error) && meta.touched : false;
                            return (
                                <FormControl style={{ minWidth: '120px' }} error={showError}>
                                    <InputLabel id="metadata-type">Type</InputLabel>
                                    <Select
                                        labelId="metadata-type"
                                        id="metadata-type-select"
                                        value={value}
                                        onChange={(event) => {
                                            setValue(`${field.name}.value`, null);
                                            onChange(event.target.value);
                                        }}
                                    >
                                        <MenuItem value={AssetMetadataTypes.Number}>Number</MenuItem>
                                        <MenuItem value={AssetMetadataTypes.String}>String</MenuItem>
                                        <MenuItem value={AssetMetadataTypes.Boolean}>Boolean</MenuItem>
                                        <MenuItem value={AssetMetadataTypes.Date}>Date</MenuItem>
                                        <MenuItem value={AssetMetadataTypes.Image}>Image</MenuItem>
                                    </Select>
                                    {showError ? <FormHelperText>{meta.error || meta.submitError}</FormHelperText> : null}
                                </FormControl>
                            );
                        }}
                    />
                    {valueSelector}
                </div>
                <div className={classes.leftRightPadding}>
                    <Tooltip title="Delete value" aria-label="Delete value">
                        <IconButton color="secondary" onClick={removeSelf} style={{ alignSelf: 'flex-end' }} size="small">
                            <DeleteIcon />
                        </IconButton>
                    </Tooltip>
                </div>
            </div>
        </Paper>
    );
};

export default AssetMetdataFieldEditor;
