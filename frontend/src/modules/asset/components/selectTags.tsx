import { makeStyles } from '@material-ui/core';
import React from 'react';
import { KeyboardEventHandler, ValueType } from 'react-select';
import CreatableSelect from 'react-select/creatable';
import { formStyles } from '../../../components/formStyles';

type selectType = {
    value: string;
    label: string;
};

const useFormStyles = makeStyles(formStyles);

const SelectTags: React.FunctionComponent<{
    value: string[];
    onChange: (event: any) => void;
    label: React.ReactNode;
    meta?: any;
}> = ({ value, onChange, label }) => {
    const formClasses = useFormStyles({});

    const internalOptions: selectType[] = value.map((value) => ({ value: value, label: value }));

    const handleKeyDown: KeyboardEventHandler = (event) => {
        switch (event.key) {
            case 'Enter':
                // event.preventDefault(); //FIXME: pressing enter when an option cant be created submits the form
                break;
        }
    };

    const selectProps = {
        value: internalOptions,
        isClearable: true,
        closeMenuOnSelect: false,
        blurInputOnSelect: false,
        onKeyDown: handleKeyDown,
        options: [],
        onChange: (options: ValueType<selectType, true>) => {
            if (options) {
                onChange(options.map((option) => option.value.toLowerCase()));
            } else {
                onChange(null);
            }
        },
        styles: {
            clearIndicator: (provided: any) => ({
                ...provided,
                padding: '0px',
                margin: '8px',
            }),
        },
    };

    return (
        <div className={formClasses.formField}>
            {label}
            <CreatableSelect isMulti {...selectProps} />
        </div>
    );
};

export default SelectTags;
