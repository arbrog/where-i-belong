import { Button, Chip, makeStyles, Theme, Tooltip, Typography } from '@material-ui/core';
import EditIcon from '@material-ui/icons/Edit';
import SaveIcon from '@material-ui/icons/Save';
import CloseIcon from '@material-ui/icons/Close';
import React from 'react';
import { Field, Form } from 'react-final-form';
import { composeValidators } from '../../../components/finalFormValidators';
import { formStyles } from '../../../components/formStyles';
import { useStoreDispatch } from '../../../state/store';
import { FetchAsset, SetAssetTagsCall } from '../state/actions';
import SelectTags from './selectTags';

const useStyles = makeStyles((theme: Theme) => ({
    root: {
        padding: theme.spacing(1, 0),
    },
    mainTitle: { color: theme.palette.text.primary },
    chipContainer: {
        display: 'flex',
        justifyContent: 'center',
        flexWrap: 'wrap',
        '& > *': {
            margin: theme.spacing(0.5),
        },
    },
    extendedIcon: {
        marginRight: theme.spacing(1),
    },
    fitContent: {
        height: 'fit-content',
    },
    titleContainer: {
        display: 'flex',
        alignItems: 'center',
    },
    startIconNoMargin: {
        margin: theme.spacing(0),
    },
}));

const useFormStyles = makeStyles(formStyles);

const RenderTagsAndEdit: React.FunctionComponent<{ tags: string[]; id: string }> = ({ tags, id }) => {
    const classes = useStyles({});
    const formClassesStyle = useFormStyles({});
    const dispatch = useStoreDispatch();
    const [editing, setEditing] = React.useState(false);

    const submit = async ({ tags }: { tags: string[] }) => {
        if (await dispatch(SetAssetTagsCall({ id: id, tags }))) {
            dispatch(FetchAsset({ id }));
        }
    };
    const title = (
        <Typography variant="h5" className={classes.mainTitle} style={{ paddingRight: '1rem' }}>
            Tags:
        </Typography>
    );

    const titleButton = editing ? (
        <>
            <Tooltip title="Save Tags" arrow className={classes.extendedIcon}>
                <div className={classes.fitContent}>
                    <Button
                        classes={{ startIcon: classes.startIconNoMargin }}
                        startIcon={<SaveIcon />}
                        variant="outlined"
                        aria-label="save"
                        size="small"
                        color="primary"
                        type="submit"
                    />
                </div>
            </Tooltip>
            <Tooltip title="Cancel Edit" arrow className={classes.extendedIcon}>
                <div className={classes.fitContent}>
                    <Button
                        classes={{ startIcon: classes.startIconNoMargin }}
                        startIcon={<CloseIcon />}
                        variant="outlined"
                        aria-label="cancel"
                        size="small"
                        color="secondary"
                        onClick={() => setEditing(false)}
                    />
                </div>
            </Tooltip>
        </>
    ) : (
        <Tooltip title="Edit Tags" arrow className={classes.extendedIcon}>
            <div className={classes.fitContent}>
                <Button
                    classes={{ startIcon: classes.startIconNoMargin }}
                    startIcon={<EditIcon />}
                    variant="outlined"
                    aria-label="edit"
                    size="small"
                    color="primary"
                    onClick={() => setEditing(true)}
                />
            </div>
        </Tooltip>
    );

    const titleHeader = (
        <div className={classes.titleContainer}>
            {title}
            {titleButton}
        </div>
    );

    return (
        <div className={classes.root}>
            {editing ? (
                <Form
                    initialValues={{ tags }}
                    onSubmit={submit}
                    render={({ handleSubmit }) => (
                        <>
                            <form onSubmit={handleSubmit}>
                                {titleHeader}
                                <Field className={formClassesStyle.formField} name="tags" validate={composeValidators()}>
                                    {({ input: { value, onChange }, label, meta }) => <SelectTags value={value} onChange={onChange} label={label} meta={meta} />}
                                </Field>
                            </form>
                        </>
                    )}
                />
            ) : (
                <>
                    {titleHeader}
                    <Typography variant="h6" className={classes.mainTitle} style={{ paddingLeft: '3rem' }}>
                        <div className={classes.chipContainer}>
                            {(tags || []).map((tag) => (
                                <Chip key={tag} label={tag} color="secondary" />
                            ))}
                        </div>
                    </Typography>
                </>
            )}
        </div>
    );
};

export default RenderTagsAndEdit;
