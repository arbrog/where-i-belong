import { makeStyles } from '@material-ui/core';
import React from 'react';
import { Field } from 'react-final-form';
import { composeValidators, required, minLen } from '../../../components/finalFormValidators';
import { formStyles } from '../../../components/formStyles';
import TextFieldWrapper from '../../../components/textFieldFinalFormWrapper';
import SelectAsset from './selectAsset';
import SelectTags from './selectTags';

const useFormStyles = makeStyles(formStyles);

const EditAssetData: React.FunctionComponent = () => {
    const formClasses = useFormStyles({});
    return (
        <>
            <Field className={formClasses.formField} label="Asset Name" name="name" validate={composeValidators(required, minLen(1))} component={TextFieldWrapper} />
            <Field className={formClasses.formField} name="parent" label="Parent" validate={composeValidators()}>
                {({ input: { value, onChange }, label, meta }) => <SelectAsset value={value} onChange={onChange} label={label} meta={meta} />}
            </Field>
            <Field className={formClasses.formField} name="tags" label="Tags" validate={composeValidators()}>
                {({ input: { value, onChange }, label, meta }) => <SelectTags value={value} onChange={onChange} label={label} meta={meta} />}
            </Field>
        </>
    );
};

export default EditAssetData;
