import { Button, Divider, makeStyles, Theme } from '@material-ui/core';
import React from 'react';
import { grey } from '@material-ui/core/colors';
import { Form } from 'react-final-form';
import Tooltip from '@material-ui/core/Tooltip';
import { AddAssetInput } from '../../../generated/graphql';
import EditAssetData from './editAssetData';
import { formStyles } from '../../../components/formStyles';
import { useHistory } from 'react-router';

const useStyles = makeStyles((theme: Theme) => ({
    mainTitle: { marginBottom: 12, color: theme.palette.text.secondary },
    root: {
        flexGrow: 1,
        padding: theme.spacing(2),
    },
    form: {
        padding: theme.spacing(4),
        marginTop: theme.spacing(6),
        border: `1px solid ${grey[400]}`,
        borderRadius: theme.spacing(0.75),
        backgroundColor: theme.palette.background.default,
    },
    divider: { width: '100%', marginBottom: theme.spacing(2), marginTop: theme.spacing(2) },
}));

const useFormStyles = makeStyles(formStyles);

const CreateAssetForm: React.FunctionComponent<{ submit: (values: AddAssetInput, goBack: boolean) => Promise<void>; initialFormValues: AddAssetInput; formClasses?: string }> = ({
    submit,
    initialFormValues,
    formClasses,
}) => {
    const classes = useStyles({});
    const formClassesStyle = useFormStyles({});
    const history = useHistory();
    const [goBackClicked, setGoBackClicked] = React.useState(false);

    //TODO: not convinced that this will work consistently
    const submitWrapper = React.useCallback(
        (values: AddAssetInput) => {
            submit(values, goBackClicked);
        },
        [goBackClicked, submit],
    );

    return (
        <Form
            initialValues={initialFormValues}
            onSubmit={submitWrapper}
            render={({ handleSubmit }) => (
                <form className={formClasses} onSubmit={handleSubmit}>
                    <EditAssetData />
                    <Divider className={classes.divider} />

                    <div className={formClassesStyle.submissionButtons}>
                        <Tooltip title="Submit" aria-label="Submit">
                            <Button type="submit" color="primary" variant="contained" onClick={() => setGoBackClicked(false)}>
                                Submit
                            </Button>
                        </Tooltip>
                        <Tooltip title="Submit and Go Back" aria-label="Submit and Go Back">
                            <Button type="submit" color="primary" variant="outlined" onClick={() => setGoBackClicked(true)}>
                                Submit and Go Back
                            </Button>
                        </Tooltip>
                        <Tooltip title="Cancel" aria-label="Cancel">
                            <Button color="secondary" variant="outlined" onClick={() => history.goBack()}>
                                Cancel
                            </Button>
                        </Tooltip>
                    </div>
                </form>
            )}
        />
    );
};

export default CreateAssetForm;
