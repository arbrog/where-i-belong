import { makeStyles, Theme } from '@material-ui/core';
import React, { Suspense } from 'react';
import { Route, Switch } from 'react-router';
import CenterLoader from '../../components/CenterLoader';

const ViewAsset = React.lazy(() => import('./viewAsset'));
const NodeView = React.lazy(() => import('./assetNodeView'));
const CreateAsset = React.lazy(() => import('./createAsset'));
const EditAsset = React.lazy(() => import('./editAsset'));

const useStyles = makeStyles((theme: Theme) => ({
    container: {
        padding: theme.spacing(3),
    },
}));

const Assets: React.FunctionComponent = () => {
    const classes = useStyles({});
    return (
        <div className={classes.container}>
            <Suspense fallback={<CenterLoader />}>
                <Switch>
                    <Route key="viewAsset" path={`/assets/asset/:assetId`} component={ViewAsset} />
                    <Route key="assetNodeView" path={`/assets/assetTree`} component={NodeView} />
                    <Route key="createAsset" path={`/assets/createAsset`} component={CreateAsset} />
                    <Route key="editAsset" path={`/assets/editAsset/:assetId`} component={EditAsset} />
                </Switch>
            </Suspense>
        </div>
    );
};

export default Assets;
