import {
    makeStyles,
    Theme,
    Typography,
    Grid,
    CircularProgress,
    Fab,
    Divider,
    Tooltip,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    List,
    Button,
    Box,
} from '@material-ui/core';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../state/rootReducer';
import { ProvidePageTitle } from '../base/title';
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import { useHistory, useParams } from 'react-router';
import AddIcon from '@material-ui/icons/Add';
import { DeleteAssetCall, FetchAsset, FETCH_ASSET_LOADING_INDICATOR } from './state/actions';
import AssetCard from './components/assetCard';
import { Link } from 'react-router-dom';
import ChangeOwnerDialog from './components/changeOwnerDialog';
import { ModifyAssetInput } from '../../generated/graphql';
import SplitButton from '../../components/splitButton';
import ShowQRCode from './components/showQRCode';
import CameraAltIcon from '@material-ui/icons/CameraAlt';
import AssetPermissionSelector from './components/assetPermissionSelector';
import SelectPermissionTarget from './components/selectPermissionTarget';
import { IAsset } from './state/reducer';
import RenderTagsAndEdit from './components/renderTagsAndEdit';
import { MetadataField, metadataValueRenderer } from './util/metadataTypes';

const useStyles = makeStyles((theme: Theme) => ({
    container: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
    },
    root: {
        flexGrow: 1,
        padding: theme.spacing(2),
    },
    padCenter: { padding: theme.spacing(0.5), alignSelf: 'center' },
    titleContainer: { display: 'flex', flexDirection: 'row', alignSelf: 'flex-start' },
    mainTitle: { color: theme.palette.text.primary },
    secondaryTitle: { marginLeft: theme.spacing(1.5), color: theme.palette.text.disabled },
    loaderRoot: {
        display: 'flex',
        justifyContent: 'center',
        width: '100%',
    },
    progress: {
        margin: theme.spacing(2),
    },
    divider: { width: '100%', marginBottom: theme.spacing(5), marginTop: theme.spacing(3) },
    extendedIcon: {
        marginRight: theme.spacing(1),
    },
    fitContent: {
        height: 'fit-content',
    },
    permissionsRoot: {
        backgroundColor: theme.palette.background.default,
    },
    assetInfoBox: {
        alignSelf: 'flex-start',
    },
    assetInfoSubSection: {
        width: 'fit-content',
    },
}));

const ViewAsset: React.FunctionComponent = () => {
    const classes = useStyles({});
    const dispatch = useDispatch();
    const history = useHistory();
    const { assetId } = useParams<{ assetId: string }>();
    const userId = useSelector((state: RootState) => state.profile.user?.id);
    const loading = useSelector((state: RootState) => state.loading.actions[FETCH_ASSET_LOADING_INDICATOR]?.loading);
    const { asset } = useSelector((state: RootState) => state.asset);
    const [editParent, setEditParent] = React.useState(false);
    const [showQRCode, setShowQRCode] = React.useState(false);
    const [parentData, setParentData] = React.useState<ModifyAssetInput>({ parent: null });
    const [openAddPermission, setOpenAddPermission] = React.useState(false);

    React.useEffect(() => {
        dispatch(FetchAsset({ id: assetId }));
    }, [dispatch, assetId]);

    const options = [
        { name: 'Edit Asset', onclick: () => history.push(`/assets/editAsset/${asset?.id}`), disabled: loading },
        {
            name: `${asset?.parent ? 'Change' : 'Set'} Asset Parent`,
            onclick: () => {
                setParentData({ parent: asset?.parent?.id });
                setEditParent(true);
            },
            disabled: loading,
        },
        {
            name: 'Delete Asset',
            onclick: async () => {
                await dispatch(DeleteAssetCall({ id: assetId }));
                history.push('/searchAssets/');
            },
            disabled: loading,
        },
    ];

    if (!asset || loading) {
        return (
            <div className={classes.container}>
                <CircularProgress />
            </div>
        );
    }
    const parentFab = (
        <Fab aria-label="open" disabled={!asset?.parent} size="small" color="secondary">
            <ArrowUpwardIcon />
        </Fab>
    );

    const TooltipExt = (props: any) => {
        return !props.disabled ? <Tooltip {...props}>{props.children}</Tooltip> : <>{props.children}</>;
    };

    return (
        <div className={classes.container}>
            <ProvidePageTitle>Asset</ProvidePageTitle>
            <div style={{ display: 'flex', justifyContent: 'space-between', width: '100%' }}>
                <div style={{ alignSelf: 'flex-start' }}>
                    <div className={classes.titleContainer}>
                        <Typography variant="h4" className={classes.mainTitle}>
                            {asset.name}
                        </Typography>
                        <Typography variant="h6" className={classes.secondaryTitle} style={{ alignSelf: 'flex-end' }}>
                            {`by ${asset.owner.firstName}`}
                        </Typography>
                    </div>
                    <Typography className={classes.secondaryTitle} color="textSecondary" gutterBottom>
                        Last Updated on {asset.updated_at}
                    </Typography>
                </div>
                <div style={{ display: 'flex', alignItems: 'baseline' }}>
                    <Tooltip title="Go To Parent" arrow className={classes.extendedIcon}>
                        <div className={classes.fitContent}>
                            {asset?.parent ? (
                                <Link
                                    to={`/assets/asset/${asset?.parent?.id}`}
                                    key={asset?.parent?.id}
                                    style={{ textDecoration: 'none' }}
                                    onClick={(e) => (asset?.parent ? null : e.preventDefault())}
                                >
                                    {parentFab}
                                </Link>
                            ) : (
                                parentFab
                            )}
                        </div>
                    </Tooltip>
                    <Tooltip title="Show QR Code" arrow className={classes.extendedIcon}>
                        <div className={classes.fitContent}>
                            <Fab aria-label="open" size="small" color="primary" onClick={() => setShowQRCode(true)}>
                                <CameraAltIcon />
                            </Fab>
                        </div>
                    </Tooltip>
                    <SplitButton label="Edit Asset" options={options} />
                </div>
            </div>
            <ChangeOwnerDialog open={editParent} change={!!asset?.parent} handleClose={() => setEditParent(false)} initialValues={parentData} id={assetId} />
            <ShowQRCode open={showQRCode} handleClose={() => setShowQRCode(false)} asset={asset} />
            <Box className={classes.assetInfoBox}>
                <Box p={1} className={classes.assetInfoSubSection}>
                    <Typography variant="h6" className={classes.mainTitle}>
                        Parent:{' '}
                        {asset?.parent?.id ? (
                            <Link key={asset?.parent?.id} to={`/assets/asset/${asset?.parent?.id}`}>
                                {asset?.parent?.name}
                            </Link>
                        ) : (
                            'None'
                        )}
                    </Typography>
                </Box>
                <Box p={1} className={classes.assetInfoSubSection}>
                    <RenderTagsAndEdit tags={(asset.tags || []) as string[]} id={asset.id} />
                </Box>
                <Box p={1} className={classes.assetInfoSubSection}>
                    <Typography variant="h5" className={classes.mainTitle}>
                        Metadata:
                    </Typography>
                    <Box p={1}>
                        <Typography variant="h6" className={classes.mainTitle} style={{ paddingLeft: '3rem' }}>
                            {asset.metadata ? (
                                <TableContainer component={Paper}>
                                    <Table aria-label="metadata table" size="small">
                                        <TableHead>
                                            <TableRow>
                                                <TableCell>
                                                    <b>Key</b>
                                                </TableCell>
                                                <TableCell>
                                                    <b>Type</b>
                                                </TableCell>
                                                <TableCell align="right">
                                                    <b>Value</b>
                                                </TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            {Object.entries(asset.metadata as Record<string, MetadataField>).map(([key, metadata]) => (
                                                <TableRow key={key}>
                                                    <TableCell component="th" scope="row">
                                                        {key}
                                                    </TableCell>
                                                    <TableCell component="th" scope="row">
                                                        {metadata.type}
                                                    </TableCell>
                                                    <TableCell align="right">{metadataValueRenderer(metadata, key)}</TableCell>
                                                </TableRow>
                                            ))}
                                        </TableBody>
                                    </Table>
                                </TableContainer>
                            ) : (
                                'This asset has no metadata'
                            )}
                        </Typography>
                    </Box>
                </Box>
                <Box p={1} className={classes.assetInfoSubSection}>
                    <div style={{ display: 'flex' }}>
                        <div>
                            <Typography variant="h5" className={classes.mainTitle}>
                                Shared With:
                            </Typography>
                        </div>
                        <div>
                            <TooltipExt title="Add Entry" disabled={userId !== asset.owner.id}>
                                <Button size="small" color="primary" aria-label="Add" onClick={() => setOpenAddPermission((value) => !value)} disabled={userId !== asset.owner.id}>
                                    <AddIcon />
                                </Button>
                            </TooltipExt>
                            <SelectPermissionTarget open={openAddPermission} asset={asset as IAsset} handleClose={() => setOpenAddPermission(false)} />
                        </div>
                    </div>
                    {asset.permissions && asset.permissions?.length > 0 ? (
                        <div className={classes.permissionsRoot}>
                            <List>
                                {/* TODO: fix typing */}

                                {Object.values(asset.permissions).map((permission) => (
                                    <AssetPermissionSelector key={permission.target.email} permission={permission as any} assetId={asset.id} disabled={userId !== asset.owner.id} />
                                ))}
                            </List>
                        </div>
                    ) : undefined}
                </Box>
            </Box>
            <div style={{ display: 'flex' }}>
                <div className={classes.padCenter}>
                    <Typography variant="h5" className={classes.mainTitle}>
                        Children
                    </Typography>
                </div>
                <div className={classes.padCenter}>
                    <Tooltip title="Add Child Asset">
                        <div>
                            <Link to={`/assets/createAsset?parent=${asset?.id}`} key={asset?.parent?.id} style={{ textDecoration: 'none' }}>
                                <Fab aria-label="open" size="small" color="primary">
                                    <AddIcon />
                                </Fab>
                            </Link>
                        </div>
                    </Tooltip>
                </div>
            </div>
            <Divider className={classes.divider} />
            <Grid container className={classes.root} spacing={2}>
                <Grid item xs={12}>
                    <Grid container justify="center" spacing={4}>
                        {asset && asset.children && asset.children.length > 0 ? (
                            asset.children.map((assetChild) => {
                                return <AssetCard asset={assetChild} key={assetChild.id} />;
                            })
                        ) : (
                            <Typography variant="h6" className={classes.mainTitle}>
                                This Asset Has No Children
                            </Typography>
                        )}
                    </Grid>
                </Grid>
            </Grid>
        </div>
    );
};

export default ViewAsset;
