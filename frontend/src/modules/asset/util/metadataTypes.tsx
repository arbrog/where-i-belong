import * as React from 'react';
import { ReactNode } from 'react';
import { v4 as uuidv4 } from 'uuid';
import { AssetMetadataInput, AssetMetadataTypes } from '../../../generated/graphql';
import { useStoreDispatch } from '../../../state/store';
import { PutImageURIAction, PutImageAction, RemoveImageAction } from '../../storage/state/actions';
import StorageImageRenderer from '../components/storageImageRenderer';

//TODO: Create type cast/extraction function
export type MetadataField = {
    type: AssetMetadataTypes;
    data: any;
    created_at: Date;
    updated_at: Date;
};

export type ImageUploadPayload = {
    file?: File;
    currentImage?: string;
};

export function metadataValueRenderer(field: MetadataField, fieldKey: string): ReactNode {
    switch (field.type) {
        case AssetMetadataTypes.Boolean:
            return field.data ? 'True' : 'False';
        case AssetMetadataTypes.Image:
            return <StorageImageRenderer field={field} fieldKey={fieldKey} />;
        default:
            return field.data;
    }
}

export function metadataFormIntialValue(field: AssetMetadataTypes, fieldData: any): any {
    switch (field) {
        case AssetMetadataTypes.Image:
            return { currentImage: fieldData } as ImageUploadPayload;
        default:
            return fieldData;
    }
}

export const uniqueImageNameParser = (uniqueName: string) => {
    const splitString = uniqueName.split('_');
    return splitString.slice(0, splitString.length).join();
};

export async function metadataFieldSubmitPrep(field: AssetMetadataInput, dispatch: ReturnType<typeof useStoreDispatch>, pushSubmissionStage: (input: string) => void) {
    switch (field.type) {
        case AssetMetadataTypes.Image:
            const data: ImageUploadPayload = field.data;
            if (data?.currentImage && data?.file) {
                //delete old image
                pushSubmissionStage(`Removing old image ${field.name}`);
                await dispatch(RemoveImageAction({ name: data.currentImage }));
                pushSubmissionStage(`Removed old image ${field.name}`);
            }
            if (data?.file) {
                //upload new image
                const fileNameSplit = data.file.name.split('.');
                const fileType = fileNameSplit.slice(fileNameSplit.length - 1, fileNameSplit.length);
                const uniqueName = `${field.name}_${uuidv4()}.${fileType}`;
                pushSubmissionStage(`Getting upload URL for ${field.name}`);
                const putURI = await dispatch(PutImageURIAction({ name: uniqueName }));
                pushSubmissionStage(`Got upload URL for ${field.name}`);
                pushSubmissionStage(`Uploading image for ${field.name}`);
                await dispatch(PutImageAction({ putURI, file: data.file }));
                pushSubmissionStage(`Uploaded image for ${field.name}`);
                return { ...field, data: uniqueName };
            }

            return { ...field, data: field.data.currentImage };
        default:
            return field;
    }
}
