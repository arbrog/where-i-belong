import { actionCreator } from '../../../state/utils';

export const SetNavExpandedActionCreator = actionCreator<'SET_NAV_EXPANDED', boolean>('SET_NAV_EXPANDED');

export type BaseActions = ReturnType<typeof SetNavExpandedActionCreator>;
