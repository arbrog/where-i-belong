import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useSnackbar } from 'notistack';
import { RootState } from '../../state/rootReducer';
import { makeWritable } from '../../state/utils';
import { RemoveSnackbarActionCreator } from '../../state/snack/actions';

let displayed: any[] = [];

const SnackDisplay = () => {
    const dispatch = useDispatch();
    const notifications = useSelector((state: RootState) => makeWritable(state.snack.notifications || []));
    const { enqueueSnackbar, closeSnackbar } = useSnackbar();

    const storeDisplayed = (id: any) => {
        displayed = [...displayed, id];
    };

    const removeDisplayed = (id: any) => {
        displayed = [...displayed.filter((key) => id !== key)];
    };

    React.useEffect(() => {
        notifications.forEach(({ key, message, options = {}, dismissed = false }) => {
            if (dismissed) {
                // dismiss snackbar using notistack
                closeSnackbar(key);
                return;
            }

            // do nothing if snackbar is already displayed
            if (displayed.includes(key)) return;

            // display snackbar using notistack
            enqueueSnackbar(message, {
                key,
                ...(options as any),
                onExited: (event, myKey) => {
                    // remove this snackbar from redux store
                    dispatch(RemoveSnackbarActionCreator({ key: myKey as any }));
                    removeDisplayed(myKey);
                },
            });

            // keep track of snackbars that we've displayed
            storeDisplayed(key);
        });
    }, [notifications, closeSnackbar, enqueueSnackbar, dispatch]);

    return null;
};

export default SnackDisplay;
