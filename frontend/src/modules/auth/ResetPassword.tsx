import React from 'react';
import { Typography, makeStyles, Theme, TextField, Button, Divider } from '@material-ui/core';
import { ProvidePageTitle } from '../base/title';
import { Link, useParams } from 'react-router-dom';
import { enqueueSnackbar } from '../../state/snack/actions';
import { useStoreDispatch } from '../../state/store';
import { ResetPasswordAction } from './state/actions';

const useStyles = makeStyles((theme: Theme) => ({
    loaderRoot: {
        display: 'flex',
        justifyContent: 'center',
        width: '100%',
    },
    progress: {
        margin: theme.spacing(2),
    },
    formContainer: {
        padding: 16,
        paddingTop: 12,
        marginTop: 20,
        display: 'flex',
        flexDirection: 'column',
    },
    socialIcon: {
        width: 24,
    },
    inputLabel: {
        fontSize: 15,
    },
    divider: { width: '100%', marginBottom: 40 },
}));

const ResetPasswordForm: React.FunctionComponent = () => {
    const classes = useStyles({});
    const dispatch = useStoreDispatch();
    const { token } = useParams<{ token: string }>();

    const [password, setPassword] = React.useState('');
    const [resetComplete, setResetComplete] = React.useState(false);

    const resetPassword = async () => {
        if (!password || password.length < 5) {
            dispatch(enqueueSnackbar({ message: 'Password is required and must be atleast 5 characters', options: { variant: 'error' } }));
            return;
        }
        setResetComplete(await dispatch(ResetPasswordAction(token, password)));
    };

    return (
        <>
            <ProvidePageTitle>Reset Password</ProvidePageTitle>
            <Typography>Reset Password</Typography>
            {resetComplete ? (
                <Typography variant="h5" component="h2">
                    Password Reset Successful
                </Typography>
            ) : (
                <form className={classes.formContainer}>
                    <TextField
                        inputProps={{ tabIndex: 2 }}
                        // required
                        variant="outlined"
                        margin="dense"
                        name="password"
                        type="password"
                        label="New Password"
                        id="password"
                        autoComplete="current-password"
                        value={password}
                        onChange={(event) => setPassword(event.target.value)}
                    />
                    <div>
                        <Button onClick={resetPassword} variant="contained" color="primary">
                            Reset Password
                        </Button>
                    </div>
                </form>
            )}

            <Divider className={classes.divider} />
            <Link to="/login">Return To Login</Link>
        </>
    );
};
export default ResetPasswordForm;
