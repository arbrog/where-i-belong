import React, { Suspense } from 'react';
import LoginBase from './loginBase';
import { makeStyles, Theme, Paper } from '@material-ui/core';
import { Route, Switch } from 'react-router';
import CenterLoader from '../../components/CenterLoader';

const CreateUserForm = React.lazy(() => import('./CreateUserForm'));
const ResetPasswordForm = React.lazy(() => import('./ResetPassword'));
const ResetPasswordRequestForm = React.lazy(() => import('./ResetPasswordRequest'));
const LoginForm = React.lazy(() => import('./LoginForm'));

const useStyles = makeStyles((theme: Theme) => ({
    container: {
        padding: theme.spacing(3),
    },
}));

const Login: React.FunctionComponent = () => {
    const classes = useStyles({});

    return (
        <LoginBase>
            <Paper className={classes.container}>
                <Suspense fallback={<CenterLoader />}>
                    <Switch>
                        {/* Feature Flag ACCOUNT_CREATION */}
                        {(process.env.REACT_APP_ACCOUNT_CREATION || '').toLowerCase() === 'false' ? null : (
                            <Route key="createAccount" path={`/login/createAccount`} component={CreateUserForm} />
                        )}
                        <Route key="resetPassword" path={`/login/resetPassword/:token`} component={ResetPasswordForm} />
                        <Route key="resetPassword" path={`/login/resetPassword`} component={ResetPasswordRequestForm} />
                        <Route key="login" path={`/login`} component={LoginForm} />
                    </Switch>
                </Suspense>
            </Paper>
        </LoginBase>
    );
};

export default Login;
