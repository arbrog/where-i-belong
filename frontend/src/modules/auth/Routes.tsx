import React from 'react';
import { Route } from 'react-router-dom';
import { handleLogin } from './wrapper';

const Login = React.lazy(() => import('./Login'));

export const LoginRoutes = (): React.ReactNode[] => [<Route key="login" path="/login" component={handleLogin(Login)} />];
