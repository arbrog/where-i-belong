import React from 'react';
import { Typography, makeStyles, Theme, TextField, Button, Divider } from '@material-ui/core';
import { ProvidePageTitle } from '../base/title';
import { LoginUsernamePassword } from './state/actions';
import { useStore, useDispatch } from 'react-redux';
import { RootState } from '../../state/rootReducer';
import { Link } from 'react-router-dom';

const useStyles = makeStyles((theme: Theme) => ({
    loaderRoot: {
        display: 'flex',
        justifyContent: 'center',
        width: '100%',
    },
    progress: {
        margin: theme.spacing(2),
    },
    formContainer: {
        padding: 16,
        paddingTop: 12,
        marginTop: 20,
        display: 'flex',
        flexDirection: 'column',
    },
    socialIcon: {
        width: 24,
    },
    inputLabel: {
        fontSize: 15,
    },
    divider: { width: '100%', marginBottom: 40 },
}));

const LoginForm: React.FunctionComponent = () => {
    const classes = useStyles({});
    const store = useStore<RootState>();
    const dispatch = useDispatch();

    const [email, setEmail] = React.useState('');
    const [password, setPassword] = React.useState('');

    const login = async (event: any) => {
        event.preventDefault();
        LoginUsernamePassword(email, password)(dispatch, store.getState, undefined);
    };
    return (
        <>
            <ProvidePageTitle>Login</ProvidePageTitle>
            <Typography variant="h5">Login</Typography>
            <form className={classes.formContainer} onSubmit={login}>
                <TextField
                    inputProps={{ tabIndex: 1 }}
                    // required
                    variant="outlined"
                    margin="dense"
                    name="email"
                    label="Email"
                    type="email"
                    id="email"
                    autoComplete="email"
                    value={email}
                    onChange={(event) => setEmail(event.target.value)}
                />
                <TextField
                    inputProps={{ tabIndex: 2 }}
                    // required
                    variant="outlined"
                    margin="dense"
                    name="password"
                    type="password"
                    label="Password"
                    id="password"
                    autoComplete="current-password"
                    value={password}
                    onChange={(event) => setPassword(event.target.value)}
                />
                <div>
                    <Button type="submit" variant="contained" color="primary">
                        Login
                    </Button>
                </div>
            </form>
            <Divider className={classes.divider} />
            {(process.env.REACT_APP_ACCOUNT_CREATION || '').toLowerCase() === 'false' ? null : (
                <div>
                    <Link to="login/createAccount">Create Account</Link>
                </div>
            )}

            <div>
                <Link to="login/resetPassword">Forgot Your Password?</Link>
            </div>
        </>
    );
};
export default LoginForm;
