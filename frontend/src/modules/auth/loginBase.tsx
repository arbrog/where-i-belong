import { Container, Theme, Divider } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import React from 'react';
import { ReactComponent as Logo } from '../../assets/where-i-belong.svg';

const useStyles = makeStyles((_theme: Theme) => ({
    mainContainer: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        paddingBottom: 60,
    },
    logo: {
        width: 60,
        margin: 8,
        fill: '#f50057',
    },
    divider: { width: '100%', marginBottom: 40 },
    content: { display: 'flex', flexDirection: 'column', width: '100%', textAlign: 'center' },
}));

const LoginBase: React.FunctionComponent = ({ children }) => {
    const classes = useStyles();
    return (
        <Container maxWidth="xs" className={classes.mainContainer}>
            <Logo className={classes.logo} />
            <Divider className={classes.divider} />
            <div className={classes.content}>{children}</div>
        </Container>
    );
};

export default LoginBase;
