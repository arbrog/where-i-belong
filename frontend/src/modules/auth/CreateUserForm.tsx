import React from 'react';
import { Typography, makeStyles, Theme, Button, CircularProgress } from '@material-ui/core';
import { ProvidePageTitle } from '../base/title';
import { CreateAccountAction, CREATE_ACCOUNT_LOADING } from './state/actions';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../state/rootReducer';
import { Field, Form } from 'react-final-form';
import { composeValidators, isEmail, minLen, required } from '../../components/finalFormValidators';
import TextFieldWrapper from '../../components/textFieldFinalFormWrapper';

const useStyles = makeStyles((theme: Theme) => ({
    container: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
    },
    loaderRoot: {
        display: 'flex',
        justifyContent: 'center',
        width: '100%',
    },
    progress: {
        margin: theme.spacing(2),
    },
    formContainer: {
        padding: 16,
        paddingTop: 12,
        marginTop: 20,
        display: 'flex',
        flexDirection: 'column',
    },
    socialIcon: {
        width: 24,
    },
    inputLabel: {
        fontSize: 15,
    },
    formField: {
        margin: `0 ${theme.spacing(1)}px`,
        marginBottom: theme.spacing(3),
        // flex: 1,
    },
}));

const initialFormValues = {
    firstName: '',
    lastName: '',
    email: '',
    password: '',
};

type FormValues = typeof initialFormValues;

const CreateUserForm: React.FunctionComponent = () => {
    const classes = useStyles({});
    const dispatch = useDispatch();

    const loading = useSelector((state: RootState) => state.loading.actions[CREATE_ACCOUNT_LOADING]?.loading);

    const submit = (values: FormValues) => {
        dispatch(CreateAccountAction({ user: { ...values, firstName: values.firstName || '', lastName: values.lastName || '' } }));
    };

    if (loading) {
        return (
            <div className={classes.container}>
                <div className={classes.formContainer}>
                    <CircularProgress />
                </div>
            </div>
        );
    }

    return (
        <>
            <ProvidePageTitle>Create Account</ProvidePageTitle>
            <Typography>Create Account</Typography>
            <Form
                initialValues={initialFormValues}
                onSubmit={submit}
                render={({ handleSubmit }) => (
                    <form className={classes.formContainer} onSubmit={handleSubmit}>
                        <Field className={classes.formField} label="First Name" name="firstName" validate={composeValidators(required)} component={TextFieldWrapper} />
                        <Field className={classes.formField} label="Last Name" name="lastName" validate={composeValidators(required)} component={TextFieldWrapper} />
                        <Field className={classes.formField} label="Email" name="email" validate={composeValidators(required, isEmail)} component={TextFieldWrapper} />
                        <Field
                            className={classes.formField}
                            label="Password"
                            name="password"
                            type="password"
                            validate={composeValidators(required, minLen(5))}
                            component={TextFieldWrapper}
                        />
                        <Button type="submit" color="primary" variant="contained">
                            Create Account
                        </Button>
                    </form>
                )}
            />
        </>
    );
};
export default CreateUserForm;
