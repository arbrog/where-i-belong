import React from 'react';
import { Typography, makeStyles, Theme, TextField, Button, Divider } from '@material-ui/core';
import { ProvidePageTitle } from '../base/title';
import { Link } from 'react-router-dom';
import { enqueueSnackbar } from '../../state/snack/actions';
import { RequestResetPasswordAction } from './state/actions';
import { useStoreDispatch } from '../../state/store';

const useStyles = makeStyles((theme: Theme) => ({
    loaderRoot: {
        display: 'flex',
        justifyContent: 'center',
        width: '100%',
    },
    progress: {
        margin: theme.spacing(2),
    },
    formContainer: {
        padding: 16,
        paddingTop: 12,
        marginTop: 20,
        display: 'flex',
        flexDirection: 'column',
    },
    socialIcon: {
        width: 24,
    },
    inputLabel: {
        fontSize: 15,
    },
    divider: { width: '100%', marginBottom: 40 },
}));

const ResetPasswordRequestForm: React.FunctionComponent = () => {
    const classes = useStyles({});
    const dispatch = useStoreDispatch();

    const [email, setEmail] = React.useState('');
    const [requestComplete, setRequestComplete] = React.useState(false);

    const requestResetPassword = async () => {
        if (!email || email === '') {
            dispatch(enqueueSnackbar({ message: 'Email is required', options: { variant: 'error' } }));
            return;
        }
        setRequestComplete(await dispatch(RequestResetPasswordAction(email)));
    };

    return (
        <>
            <ProvidePageTitle>Request Password Reset</ProvidePageTitle>
            <Typography>Request Password Reset</Typography>
            {requestComplete ? (
                <Typography variant="h6" component="h2">
                    If an account exists with that email, an email was sent with a password reset link which is valid for 30 minutes.
                </Typography>
            ) : (
                <form className={classes.formContainer}>
                    <TextField
                        inputProps={{ tabIndex: 2 }}
                        // required
                        variant="outlined"
                        margin="dense"
                        name="email"
                        type="email"
                        label="Email"
                        id="email"
                        autoComplete="email"
                        value={email}
                        onChange={(event) => setEmail(event.target.value)}
                    />
                    <div>
                        <Button onClick={requestResetPassword} variant="contained" color="primary">
                            Request Password Reset
                        </Button>
                    </div>
                </form>
            )}

            <Divider className={classes.divider} />
            <Link to="/login">Return To Login</Link>
        </>
    );
};
export default ResetPasswordRequestForm;
