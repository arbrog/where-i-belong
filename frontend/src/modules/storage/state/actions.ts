import { actionCreator, ActionTypeHelper } from '../../../state/utils';
import { WrapLoading } from '../../../state/loading/actions';
import { GetGraphQLClient } from '../../auth/state/actions';
import {
    GetImageUri,
    GetImageUriQuery,
    GetImageUriQueryVariables,
    PutImageUri,
    PutImageUriMutation,
    PutImageUriMutationVariables,
    RemoveImage,
    RemoveImageMutation,
    RemoveImageMutationVariables,
} from '../../../generated/graphql';
import { checkCachedImageURI } from './reducer';

export const GetImageURIActionCreator = actionCreator<'GET_IMAGE_URI', GetImageUriQuery['getImageURI']>('GET_IMAGE_URI');
export const PostImageURIActionCreator = actionCreator<'POST_IMAGE_URI', PutImageUriMutation['putImageURI']>('POST_IMAGE_URI');
export const RemoveImageActionCreator = actionCreator<'REMOVE_IMAGE', RemoveImageMutation['removeImage']>('REMOVE_IMAGE');
export const PostImageActionCreator = actionCreator<'POST_IMAGE', boolean>('POST_IMAGE');

export type StorageActions =
    | ReturnType<typeof GetImageURIActionCreator>
    | ReturnType<typeof PostImageURIActionCreator>
    | ReturnType<typeof RemoveImageActionCreator>
    | ReturnType<typeof PostImageActionCreator>;

export const GET_IMAGE_URI_LOADING_INDICATOR = 'get_image_uri';

export function GetImageURIAction(args: GetImageUriQueryVariables): ActionTypeHelper<Promise<void>> {
    return WrapLoading(
        GET_IMAGE_URI_LOADING_INDICATOR,
        async (dispatch, getState) => {
            const isValid = checkCachedImageURI(args.name, getState);
            if (isValid) {
                //dont make request if valid uri already exists
                return;
            }

            const client = GetGraphQLClient()(dispatch, getState, undefined);
            const response = await client.query<GetImageUriQuery, GetImageUriQueryVariables>({
                query: GetImageUri,
                variables: args,
            });

            if (response.errors) {
                throw new Error(response.errors.map((e) => e.message).join('\n'));
            }
            const { getImageURI } = response.data;

            dispatch(GetImageURIActionCreator(getImageURI));
        },
        false,
    );
}

export const PUT_IMAGE_URI_LOADING_INDICATOR = 'put_image_uri';

export function PutImageURIAction(args: PutImageUriMutationVariables): ActionTypeHelper<Promise<string>> {
    return WrapLoading(
        GET_IMAGE_URI_LOADING_INDICATOR,
        async (dispatch, getState) => {
            const client = GetGraphQLClient()(dispatch, getState, undefined);
            const response = await client.query<PutImageUriMutation, PutImageUriMutationVariables>({
                query: PutImageUri,
                variables: args,
            });

            if (response.errors) {
                throw new Error(response.errors.map((e) => e.message).join('\n'));
            }
            const { putImageURI } = response.data;

            dispatch(PostImageURIActionCreator());
            return putImageURI;
        },
        false,
    );
}

export const REMOVE_IMAGE_LOADING_INDICATOR = 'remove_image';

export function RemoveImageAction(args: RemoveImageMutationVariables): ActionTypeHelper<Promise<boolean>> {
    return WrapLoading(
        REMOVE_IMAGE_LOADING_INDICATOR,
        async (dispatch, getState) => {
            const client = GetGraphQLClient()(dispatch, getState, undefined);
            const response = await client.query<RemoveImageMutation, RemoveImageMutationVariables>({
                query: RemoveImage,
                variables: args,
            });

            if (response.errors) {
                throw new Error(response.errors.map((e) => e.message).join('\n'));
            }
            const { removeImage } = response.data;

            dispatch(RemoveImageActionCreator());
            return removeImage;
        },
        false,
    );
}

export const PUT_IMAGE_LOADING_INDICATOR = 'put_image';

export function PutImageAction(args: { file: File; putURI: string }): ActionTypeHelper<Promise<boolean>> {
    return WrapLoading(
        PUT_IMAGE_LOADING_INDICATOR,
        async (dispatch) => {
            const response = await fetch(args.putURI, {
                method: 'PUT',
                body: args.file,
            });

            if (response.status !== 200) {
                throw new Error('Failed to persist file');
            }
            dispatch(PostImageActionCreator());
            return true;
        },
        false,
    );
}
