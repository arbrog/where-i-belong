import * as React from 'react';
import { FieldRenderProps } from 'react-final-form';
import RootRef from '@material-ui/core/RootRef';
import { Box, Button, CircularProgress, FormControl, FormHelperText, makeStyles, Paper, Theme, Tooltip, Typography } from '@material-ui/core';
import { useDropzone } from 'react-dropzone';
import { GetImageURIAction, GET_IMAGE_URI_LOADING_INDICATOR } from '../state/actions';
import { getCachedImageURI } from '../state/reducer';
import { useSelector } from 'react-redux';
import { RootState } from '../../../state/rootReducer';
import { ImageUploadPayload } from '../../asset/util/metadataTypes';
import { useStoreDispatch } from '../../../state/store';

const useStyles = makeStyles((theme: Theme) => ({
    root: {
        flexGrow: 1,
        padding: theme.spacing(2),
    },
    thumbnailContainer: {
        width: theme.spacing(20),
    },
    thumbnailImage: {
        width: '100%',
    },
    flexBox: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
    },
    leftButtonPad: {
        paddingLeft: theme.spacing(1),
    },
}));

export const customImageRequired = (value: ImageUploadPayload) => {
    return value.file === null ? 'Required' : undefined;
};

const ImageUploaderFinalFormWrapper: React.FunctionComponent<FieldRenderProps<ImageUploadPayload>> = ({ input: { onChange, value }, meta }) => {
    const classes = useStyles({});
    const dispatch = useStoreDispatch();
    const uri = useSelector((state: RootState) => (value.currentImage ? getCachedImageURI(value.currentImage, () => state)?.url : undefined));
    const loadingExisting = useSelector((state: RootState) => state.loading.actions[GET_IMAGE_URI_LOADING_INDICATOR]?.loading);

    React.useEffect(() => {
        if (value.currentImage) {
            dispatch(GetImageURIAction({ name: value.currentImage }));
        }
    }, [dispatch, value.currentImage]);

    const { fileRejections, getRootProps, getInputProps } = useDropzone({
        maxSize: 1e9,
        maxFiles: 1,
        accept: 'image/jpeg, image/png',
        onDrop: (acceptedFiles) => {
            //set form value to be file object (file path)
            onChange({ ...value, file: acceptedFiles[0] });
        },
    });
    const { ref, ...rootProps } = getRootProps();

    const fileRejectionItems = fileRejections.map(({ file, errors }) => (
        <li key={file.name}>
            {file.name} - {file.size} bytes: {errors.map((e) => e.message)}
        </li>
    ));

    const thumbnailSource = value.file ? { uri: URL.createObjectURL(value.file), name: value.file.name } : { uri: uri as string, name: value.currentImage };
    const thumbnail = (
        <Box key={thumbnailSource.name} className={classes.thumbnailContainer} p={0.25}>
            <img src={thumbnailSource.uri} className={classes.thumbnailImage} />
        </Box>
    );

    const showError = Boolean((((meta.submitError && !meta.dirtySinceLastSubmit) || meta.error) && meta.touched) || fileRejectionItems);

    return (
        <FormControl required error={showError}>
            <RootRef rootRef={ref}>
                <Box p={1} className={classes.flexBox}>
                    <Paper {...rootProps} elevation={3}>
                        {loadingExisting ? (
                            <CircularProgress />
                        ) : value.file || (uri && value.file !== null) ? (
                            <Box>{thumbnail}</Box>
                        ) : (
                            <Box p={2}>
                                <input {...getInputProps()} />
                                <Typography variant="body1">Drag 'n' drop some files here, or click to select files</Typography>
                                <Typography variant="subtitle2">(Only *.jpeg and *.png images will be accepted)</Typography>
                            </Box>
                        )}
                    </Paper>
                    {value.file || (uri && value.file !== null) ? (
                        <div className={classes.leftButtonPad}>
                            <Tooltip title="Clear image" aria-label="Clear image">
                                <Button color="secondary" onClick={() => onChange({ currentImage: value.currentImage, file: null })}>
                                    Clear
                                </Button>
                            </Tooltip>
                        </div>
                    ) : null}
                </Box>
            </RootRef>
            {showError ? <FormHelperText>{meta.error || meta.submitError || fileRejectionItems}</FormHelperText> : null}
        </FormControl>
    );
};

export default ImageUploaderFinalFormWrapper;
