import { initialActions as authActions } from '../modules/auth/state/actions';

export const mergedInitialActions = [...authActions];
