import * as React from 'react';
import { FieldRenderProps } from 'react-final-form';
import { Checkbox, FormControl, FormControlLabel, FormHelperText } from '@material-ui/core';

const CheckboxFieldWrapper: React.FunctionComponent<FieldRenderProps<boolean>> = ({ input: { name, onChange, checked }, meta, ...rest }) => {
    const showError = ((meta.submitError && !meta.dirtySinceLastSubmit) || meta.error) && meta.touched;

    React.useEffect(() => {
        if (meta.initial === undefined) {
            onChange(false);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <FormControl required error={showError}>
            <FormControlLabel
                {...rest}
                control={
                    <Checkbox
                        checked={checked}
                        onChange={(event) => {
                            console.log('checkbox changed!');
                            onChange(event.target.checked);
                        }}
                        name={name}
                        color="primary"
                    />
                }
                label="Is True"
            />
            {showError ? <FormHelperText>{meta.error || meta.submitError}</FormHelperText> : null}
        </FormControl>
    );
};

export default CheckboxFieldWrapper;
