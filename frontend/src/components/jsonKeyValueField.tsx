import * as React from 'react';
import { Field } from 'react-final-form';
import DeleteIcon from '@material-ui/icons/Delete';
import { composeValidators, required } from './finalFormValidators';
import TextFieldWrapper from './textFieldFinalFormWrapper';
import { Tooltip, IconButton, makeStyles, Theme } from '@material-ui/core';
import { formStyles } from './formStyles';

const useStyles = makeStyles((_theme: Theme) => ({
    fieldGroupcontainer: {
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
}));

const useFormStyles = makeStyles(formStyles);

const JSONKeyValueField: React.FunctionComponent<{ field: { name: string; index: number }; removeSelf: () => void }> = ({ field, removeSelf }) => {
    const formClasses = useFormStyles({});
    const classes = useStyles({});

    return (
        <div className={classes.fieldGroupcontainer}>
            <Field className={formClasses.formField} label="Field Name" name={`${field.name}.name`} validate={composeValidators(required)} component={TextFieldWrapper} />
            <Field className={formClasses.formField} label="Field Value" name={`${field.name}.value`} validate={composeValidators(required)} component={TextFieldWrapper} />
            <div>
                <Tooltip title="Delete value" aria-label="Delete value">
                    <IconButton color="secondary" onClick={removeSelf} style={{ alignSelf: 'flex-end' }} size="small">
                        <DeleteIcon />
                    </IconButton>
                </Tooltip>
            </div>
        </div>
    );
};

export default JSONKeyValueField;
