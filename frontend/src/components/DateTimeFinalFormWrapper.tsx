import * as React from 'react';
import { FieldRenderProps } from 'react-final-form';
import { DateTimePicker } from '@material-ui/pickers';
import { Typography } from '@material-ui/core';

//TODO: improve errors
const DateTimeWrapper: React.FunctionComponent<FieldRenderProps<string>> = ({ input: { onChange, value, ..._restInput }, meta, ...rest }) => {
    const showError = ((meta.submitError && !meta.dirtySinceLastSubmit) || meta.error) && meta.touched;

    //on mount set default date if doesn't exist
    React.useEffect(() => {
        if (value === '' || value === null || value === undefined) {
            onChange(new Date());
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <div style={{ display: 'inline-flex', flexDirection: 'column' }}>
            <DateTimePicker {...rest} invalidLabel={meta.error || meta.submitError} style={{ marginBottom: '0' }} value={value === '' ? new Date() : value} onChange={onChange} />
            {showError ? (
                <div style={{ marginLeft: '8px' }}>
                    <Typography variant="subtitle2" color="error">
                        {meta.error || meta.submitError}
                    </Typography>
                </div>
            ) : null}
        </div>
    );
};

export default DateTimeWrapper;
