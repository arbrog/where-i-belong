import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';
import * as compression from 'compression';
import helmet from 'helmet';
import { readFileSync } from 'fs';
// eslint-disable-next-line @typescript-eslint/no-var-requires
require('dotenv').config();

//TODO: add csrf protection and rate limiting

async function bootstrap() {
    let httpsOptions: { key: Buffer; cert: Buffer };
    if (process.env.HTTPS) {
        if (!process.env.SSL_KEY_FILE || !process.env.SSL_CRT_FILE) {
            throw new Error('Missing env variables SSL_KEY_FILE and SSL_CRT_FILE');
        }
        httpsOptions = {
            key: readFileSync(process.env.SSL_KEY_FILE),
            cert: readFileSync(process.env.SSL_CRT_FILE),
        };
    }
    const app = await NestFactory.create(AppModule, { httpsOptions });
    if (process.env.NODE_ENV === 'production') {
        app.use(helmet());
    }

    app.enableCors();
    app.use(compression.default());
    app.useGlobalPipes(new ValidationPipe());
    await app.listen(process.env.PORT || 3001);
}
bootstrap();
