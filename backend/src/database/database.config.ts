import { Options, IDatabaseDriver, Connection } from '@mikro-orm/core';
import { registerAs } from '@nestjs/config';

import MikroConfig from '../../mikro-orm.config';

export type DatabaseConfigShape = Partial<Options<IDatabaseDriver<Connection>>>;

export const databaseConfig = registerAs(
    'database',
    (): DatabaseConfigShape => {
        const development = process.env.NODE_ENV !== 'production' || !!process.env.DEBUG;
        const basic: DatabaseConfigShape = {
            type: 'postgresql',
            debug: development,
        };

        const poolOpts: DatabaseConfigShape['pool'] = {
            // min: 0,
            // idleTimeoutMillis: 50,
        };

        // Environment variables are set in `mirko-orm.config.ts` because migrations need them too
        const opts: DatabaseConfigShape = MikroConfig;

        return {
            ...poolOpts,
            ...opts,
            ...basic,
        };
    },
);
