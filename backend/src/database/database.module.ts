import { Options, IDatabaseDriver, Connection } from '@mikro-orm/core';
import { MikroOrmModule } from '@mikro-orm/nestjs';
import { DynamicModule, Module } from '@nestjs/common';
import { ConfigType } from '@nestjs/config';
import { BaseEntity } from 'src/BaseEntity';
import { Asset } from '../asset/asset.entity';
import { AssetPermission } from '../asset/asset_permission.entity';
import { Token } from '../user/token.entity';
import { User } from '../user/user.entity';
import { databaseConfig } from './database.config';

@Module({})
export class DatabaseModule {
    static forRoot(opts: Partial<Options<IDatabaseDriver<Connection>>> = {}): DynamicModule {
        return {
            module: DatabaseModule,
            imports: [
                MikroOrmModule.forRootAsync({
                    inject: [databaseConfig.KEY],
                    useFactory(cfg: ConfigType<typeof databaseConfig>) {
                        return {
                            ...cfg,
                            ...opts,
                            entities: [BaseEntity, User, Asset, Token, AssetPermission],
                        };
                    },
                }),
                MikroOrmModule.forFeature({ entities: [User, Asset, Token, AssetPermission] }),
            ],
            exports: [MikroOrmModule],
        };
    }
}
