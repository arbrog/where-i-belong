import { MikroOrmModule } from '@mikro-orm/nestjs';
import { Global, Module } from '@nestjs/common';
import { EmailService } from './email.service';
import { Token } from './token.entity';
import { TokenService } from './token.service';
import { User } from './user.entity';
import { UserResolver } from './user.resolver';
import { UserService } from './user.service';

@Global()
@Module({
    imports: [MikroOrmModule.forFeature({ entities: [User, Token] })],
    providers: [UserService, UserResolver, TokenService, EmailService],
    exports: [UserService],
})
export class UserModule {}
