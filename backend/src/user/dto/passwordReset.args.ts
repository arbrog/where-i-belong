import { Field, InputType } from '@nestjs/graphql';
import { IsEmail, Length } from 'class-validator';

@InputType()
export class PasswordResetInput {
    @IsEmail()
    @Field()
    email: string;
}

@InputType()
export class PasswordChangeReset {
    @Field()
    @Length(5)
    password: string;
}
