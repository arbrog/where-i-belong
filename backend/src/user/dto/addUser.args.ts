import { IsEmail, IsString, Length } from 'class-validator';
import { Field, InputType } from '@nestjs/graphql';

@InputType()
export class AddUserInput {
    @IsString()
    @Field({ nullable: true })
    firstName?: string;

    @IsString()
    @Field({ nullable: true })
    lastName?: string;

    @Field()
    @IsEmail()
    email: string;

    @Field()
    @Length(5)
    password?: string;
}
