import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import nodemailer from 'nodemailer';
@Injectable()
export class EmailService {
    constructor(private configService: ConfigService) {}

    async sendPasswordResetEmail(recipientEmail: string, firstName: string, resetLink: string): Promise<void> {
        //TODO: make platform agonstic
        const transporter = nodemailer.createTransport({
            host: 'smtp.sendgrid.net',
            port: 25,
            secure: false,
            auth: {
                user: 'apikey',
                pass: this.configService.get<string>('SEND_GRID_API'),
            },
        });

        // send mail with defined transport object
        const email = {
            from: '"Password Reset" <arbrog@gmail.com>', // sender address
            to: recipientEmail, // list of receivers
            subject: 'Password Reset', // Subject line
            html: `
                        <html>
                            <head>
                            <title></title>
                            </head>
                            <body>
                            <div data-role="module-unsubscribe" class="module" role="module" data-type="unsubscribe" style="color:#444444; font-size:12px; line-height:20px; padding:16px 16px 16px 16px; text-align:Center;" data-muid="4e838cf3-9892-4a6d-94d6-170e474d21e5">
                                <p style="font-size:28px; line-height:20px;">
                                    Hi ${firstName}
                                </p>
                                <p style="font-size:14px; line-height:20px;">
                                    A password reset link is attached below which can be used to reset your password. If you did not request a password reset please disregard this email.
                                </p>
                                <a href="${resetLink}">Reset Password</a>
                            </div>
                            </body>
                        </html>
                    `,
        };

        try {
            await transporter.sendMail(email);
        } catch (error) {
            console.error(error);
            if (error.response) {
                console.error(error.response.body);
            }
            throw error;
        }
    }
}
