import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@mikro-orm/nestjs';
import { User } from './user.entity';
import { EntityRepository } from '@mikro-orm/core';
import { Token, TokenScope } from './token.entity';

@Injectable()
export class TokenService {
    constructor(
        @InjectRepository(Token)
        private readonly tokenRepository: EntityRepository<Token>,
    ) {}

    async findOneById(id: string): Promise<Token | undefined> {
        return this.tokenRepository.findOne({ id });
    }

    async createToken(token: string, ttl: number, scope: TokenScope, user: User): Promise<Token> {
        const createdToken = new Token(token, ttl, scope, user);

        await this.tokenRepository.persistAndFlush(createdToken);
        return createdToken;
    }

    tokenIsValid(token: Token): boolean {
        if (!token.consumed && new Date(token.created_at.getTime() + 1000 * token.ttl) > new Date()) {
            return true;
        }
        return false;
    }

    async consumeToken(token: Token): Promise<boolean> {
        if (this.tokenIsValid(token)) {
            token.consumed = true;
            await this.tokenRepository.persistAndFlush(token);
            return true;
        }
        throw new Error('Failed to consume token');
    }
}
