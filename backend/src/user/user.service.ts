import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@mikro-orm/nestjs';
import { User } from './user.entity';
import { EntityRepository } from '@mikro-orm/core';
import { AddUserInput } from './dto/addUser.args';
import bcrypt from 'bcrypt';
import { PasswordResetInput } from './dto/passwordReset.args';
import { TokenScope } from './token.entity';
import { TokenService } from './token.service';
import { randomBytes, createHash } from 'crypto';
import { EmailService } from './email.service';

const rounds = 10;

@Injectable()
export class UserService {
    constructor(
        @InjectRepository(User)
        private readonly userRepository: EntityRepository<User>,
        private tokenService: TokenService,
        private emailService: EmailService,
    ) {}

    async findAll(): Promise<User[]> {
        return this.userRepository.findAll();
    }

    async findOneById(id: string): Promise<User | undefined> {
        return this.userRepository.findOne({ id });
    }

    async addUser(userInput: AddUserInput): Promise<User> {
        const user = new User(userInput);
        if (userInput.password) {
            user.password = await bcrypt.hash(userInput.password, rounds);
        }
        await this.userRepository.persistAndFlush(user);
        return user;
    }

    async requestPasswordReset(userInput: PasswordResetInput): Promise<boolean> {
        try {
            const user = await this.findOne({ email: userInput.email });
            if (!user) {
                return true;
            }
            const tokenSecret = randomBytes(32).toString('base64');
            //don't need a salt from bcrypt because this isn't a human generated password, would be a waste of CPU cycles
            const token = await this.tokenService.createToken(createHash('sha512').update(tokenSecret).digest('hex'), 3600, TokenScope.PASSWORD_RESET, user);
            //send email
            this.emailService.sendPasswordResetEmail(
                user.email,
                user.firstName,
                `http://localhost:3000/login/resetPassword/${Buffer.from(JSON.stringify({ token_id: token.id, token: tokenSecret }), 'utf-8').toString('base64')}`,
            );
            return true;
        } catch (error) {
            throw new Error('Failed to reset password, please contact support');
        }
    }

    async changePassword(userInput: User, password: string): Promise<User> {
        userInput.password = await bcrypt.hash(password, rounds);

        await this.userRepository.persistAndFlush(userInput);
        return userInput;
    }

    async findOne(input: Record<string, any>): Promise<User | undefined> {
        return this.userRepository.findOne(input);
    }
}
