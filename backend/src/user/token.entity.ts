/* eslint-disable @typescript-eslint/no-inferrable-types */
import { Entity, Property, IdentifiedReference, ManyToOne, Reference, Enum, Unique } from '@mikro-orm/core';
import { BaseEntity } from '../BaseEntity';
import { ObjectType, Field, registerEnumType } from '@nestjs/graphql';
import { User } from './user.entity';

@Entity()
@ObjectType()
export class Token extends BaseEntity {
    @Field()
    @Property()
    ttl: number;

    @Field((_type) => TokenScope)
    @Enum()
    scope: TokenScope;

    @Unique()
    @Field()
    @Property()
    token: string;

    @Field()
    @Property()
    consumed: boolean = false;

    @Field((_type) => User, { nullable: false })
    @ManyToOne({ entity: () => User, eager: true })
    user: IdentifiedReference<User, 'id'>;

    constructor(token: string, ttl: number, scope: TokenScope, user: User) {
        super();
        this.token = token;
        this.ttl = ttl;
        this.scope = scope;
        this.user = Reference.create(user);
    }
}

export enum TokenScope {
    PASSWORD_RESET = 'password_reset',
}

registerEnumType(TokenScope, {
    name: 'TokenScope',
});
