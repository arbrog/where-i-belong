import { Resolver, Query, Args, Mutation, ResolveField, Root, ID } from '@nestjs/graphql';
import { User } from './user.entity';
import { UserService } from './user.service';
import { AddUserInput } from './dto/addUser.args';
import { GqlAuthGuard } from '../auth/strategies/jwt-gql-auth.guard';
import { HttpException, HttpStatus, Inject, UseGuards } from '@nestjs/common';
import { CurrentUser } from './user.decorator';
import { Asset } from '../asset/asset.entity';
import { PasswordChangeReset, PasswordResetInput } from './dto/passwordReset.args';
import { TokenService } from './token.service';
import { TokenScope } from './token.entity';
import { createHash } from 'crypto';
import { AuthenticationError, UserInputError } from 'apollo-server-express';
import { ConfigType } from '@nestjs/config';
import { featureFlagConfig } from '../feature.config';

function sleep(ms: number) {
    return new Promise((resolve) => setTimeout(resolve, ms));
}

function getRandomInt(min: number, max: number) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min) + min); //The maximum is exclusive and the minimum is inclusive
}

@Resolver((_of: any) => User)
export class UserResolver {
    constructor(
        @Inject(featureFlagConfig.KEY)
        private featureFlags: ConfigType<typeof featureFlagConfig>,
        private userService: UserService,
        private tokenService: TokenService,
    ) {}

    @UseGuards(GqlAuthGuard)
    @Query((_returns) => User)
    async getUser(@Args('id', { type: () => ID, nullable: true }) id?: string, @Args('email', { type: () => String, nullable: true }) email?: string): Promise<User> {
        if (id && email) {
            throw new UserInputError('id and email are mutally exclusive');
        } else if (id) {
            return this.userService.findOneById(id);
        } else if (email) {
            return this.userService.findOne({ email });
        }
        throw new UserInputError('Either id or email is required');
    }

    @UseGuards(GqlAuthGuard)
    @Query((_returns) => User)
    async getSelf(@CurrentUser() user: User): Promise<User> {
        return this.userService.findOneById(user.id);
    }

    // @UseGuards(GqlAuthGuard)
    // @Query((_returns) => [User])
    // async getUsers(): Promise<User[]> {
    //     return this.userService.findAll();
    // }

    @Mutation((_returns) => User)
    async addUser(@Args('user') userInput: AddUserInput): Promise<User> {
        if (!this.featureFlags.accountCreation) {
            throw new HttpException('Feature flag not enabled', HttpStatus.NOT_IMPLEMENTED);
        }
        return this.userService.addUser(userInput);
    }

    @Mutation((_returns) => Boolean)
    async requestPasswordReset(@Args('input') input: PasswordResetInput): Promise<boolean> {
        if (!this.featureFlags.passwordResets) {
            throw new HttpException('Feature flag not enabled', HttpStatus.NOT_IMPLEMENTED);
        }
        //prevent guessing wether a user exists based on how long the request takes
        await sleep(1000 + getRandomInt(100, 500));
        return this.userService.requestPasswordReset(input);
    }

    @Mutation((_returns) => Boolean)
    async resetPassword(@Args('token') tokenMeta: string, @Args('input') input: PasswordChangeReset): Promise<boolean> {
        if (!this.featureFlags.passwordResets) {
            throw new HttpException('Feature flag not enabled', HttpStatus.NOT_IMPLEMENTED);
        }
        //TODO: refactor into service
        try {
            const userToken = JSON.parse(Buffer.from(tokenMeta, 'base64').toString('utf-8'));
            const token = await this.tokenService.findOneById(userToken.token_id);

            //if there is no token or if the token is not for a password reset, reject it
            if (!token || token.scope !== TokenScope.PASSWORD_RESET) {
                throw new Error();
            }

            //check token secret
            if (token.token !== createHash('sha512').update(userToken.token).digest('hex')) {
                throw new Error();
            }
            // consume the token so it cannot be used again
            await this.tokenService.consumeToken(token);

            //change the users password
            this.userService.changePassword(await token.user.load(), input.password);

            return true;
        } catch (error) {
            throw new Error('Failed to change password, please try requesting again');
        }
    }

    @UseGuards(GqlAuthGuard)
    @ResolveField((_type) => [Asset])
    async assets(@CurrentUser() reqUser: User, @Root() user: User) {
        if (reqUser.id === user.id) {
            return user.assets.loadItems();
        }
        throw new AuthenticationError('Assets of a user are only visible to that user');
    }
}
