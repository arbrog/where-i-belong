/* eslint-disable @typescript-eslint/no-inferrable-types */
import { Entity, Property, Unique, Index, Collection, OneToMany } from '@mikro-orm/core';
import { BaseEntity } from '../BaseEntity';
import { Exclude } from 'class-transformer';
import { ObjectType, Field } from '@nestjs/graphql';
import { AddUserInput } from './dto/addUser.args';
import { Asset } from '../asset/asset.entity';
import { Token } from './token.entity';

@Entity()
@ObjectType()
export class User extends BaseEntity {
    @Field()
    @Property()
    firstName: string;

    @Field()
    @Property()
    lastName: string;

    @Field()
    @Unique()
    @Index()
    @Property()
    email: string;

    @Exclude()
    @Property()
    password?: string;

    @Field()
    @Property()
    verified: boolean = false;

    @Field((_type) => [Asset])
    @OneToMany(() => Asset, (asset) => asset.owner)
    assets? = new Collection<Asset>(this);

    @OneToMany(() => Token, (token) => token.user)
    tokens? = new Collection<Token>(this);

    constructor(userInput: AddUserInput) {
        super();
        this.firstName = userInput.firstName;
        this.lastName = userInput.lastName;
        this.email = userInput.email;
    }
}
