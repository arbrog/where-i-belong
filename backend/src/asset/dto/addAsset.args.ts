import { IsOptional, IsString, IsUUID, Length } from 'class-validator';
import { Field, ID, InputType } from '@nestjs/graphql';
import { GraphQLJSON } from 'graphql-type-json';
import { AssetMetadataTypes } from '../asset_metadata.embed';

@InputType()
export class AddAssetInput {
    @IsString()
    @Length(1, 128)
    @Field()
    name: string;

    @IsUUID()
    @IsOptional()
    @Field((_type) => ID, { nullable: true })
    parent?: string;

    @IsOptional()
    @Field((_type) => AssetMetadataInput, { nullable: true })
    metadata?: Record<string, AssetMetadataInput>;

    @Field((_type) => [String])
    tags?: string[];
}

@InputType()
export class AssetMetadataInput {
    @IsString()
    @Length(1, 128)
    @Field((_type) => String)
    name: string;

    @IsString()
    @Field((_type) => AssetMetadataTypes)
    type: AssetMetadataTypes;

    @Field((_type) => GraphQLJSON)
    data: Record<string, any>;
}

@InputType()
export class ModifyAssetInput {
    @IsString()
    @Length(1, 128)
    @IsOptional()
    @Field((_type) => String, { nullable: true })
    name?: string;

    @IsUUID()
    @IsOptional()
    @Field((_type) => ID, { nullable: true })
    parent?: string;

    @IsOptional()
    @Field((_type) => [AssetMetadataInput], { nullable: true })
    metadata?: AssetMetadataInput[];
}
