import { Injectable } from '@nestjs/common';
import { Metadata } from './asset_metadata.embed';
import { AssetMetadataInput } from './dto/addAsset.args';

// TODO: use deep comparison or only passed changed rows to better track updated at

@Injectable()
export class AssetMetadataService {
    convertInputToMetadata(input: AssetMetadataInput[], existingMetadata?: Record<string, Metadata>) {
        return input.reduce((accum, metadata) => {
            return {
                ...accum,
                [metadata.name]: {
                    type: metadata.type,
                    data: metadata.data,
                    created_at: existingMetadata[metadata.name]?.created_at ? existingMetadata[metadata.name]?.created_at : new Date(),
                    updated_at: new Date(),
                } as Metadata,
            };
        }, {} as Record<string, Metadata>);
    }
}
