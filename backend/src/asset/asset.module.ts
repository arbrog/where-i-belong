import { MikroOrmModule } from '@mikro-orm/nestjs';
import { Global, Module } from '@nestjs/common';
import { StorageModule } from '../storage/storage.module';
import { Asset } from './asset.entity';
import { AssetResolver } from './asset.resolver';
import { AssetService } from './asset.service';
import { AssetMetadataService } from './asset_metadata.service';
import { AssetPermission } from './asset_permission.entity';
import { AssetPermissionResolver } from './asset_permission.resolver';
import { AssetPermissionService } from './asset_permission.service';

@Global()
@Module({
    imports: [StorageModule, MikroOrmModule.forFeature({ entities: [Asset, AssetPermission] })],
    providers: [AssetService, AssetResolver, AssetPermissionService, AssetPermissionResolver, AssetMetadataService],
    exports: [AssetService],
})
export class AssetModule {}
