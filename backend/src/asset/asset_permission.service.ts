import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@mikro-orm/nestjs';
import { EntityRepository } from '@mikro-orm/postgresql';
import { Asset } from './asset.entity';
import { User } from '../user/user.entity';
import { AssetPermission, AssetPermissionType } from './asset_permission.entity';

@Injectable()
export class AssetPermissionService {
    constructor(
        @InjectRepository(AssetPermission)
        private readonly assetPermissionsRepository: EntityRepository<AssetPermission>,
    ) {}

    async findAll(input?: Record<string, any>): Promise<AssetPermission[]> {
        return this.assetPermissionsRepository.find(input);
    }

    async findOne(input: Record<string, any>): Promise<AssetPermission | undefined> {
        return this.assetPermissionsRepository.findOne(input);
    }

    async addAssetPermission(permissions: AssetPermissionType[], target: User, asset: Asset): Promise<AssetPermission> {
        const assetPerm = new AssetPermission(target, asset, permissions);
        await this.assetPermissionsRepository.persistAndFlush(assetPerm);
        return assetPerm;
    }
    async updateAssetPermission(assetPermission: AssetPermission, permissions: AssetPermissionType[]): Promise<AssetPermission> {
        assetPermission.permissions = permissions;
        await this.assetPermissionsRepository.persistAndFlush(assetPermission);
        return assetPermission;
    }

    async modifyAssetPermission(assetPerm: AssetPermission, permissions: AssetPermissionType[]): Promise<AssetPermission> {
        assetPerm.permissions = permissions;
        await this.assetPermissionsRepository.persistAndFlush(assetPerm);
        return assetPerm;
    }

    async deleteAssetPermission(assetPerm: AssetPermission): Promise<AssetPermission> {
        await this.assetPermissionsRepository.removeAndFlush(assetPerm);
        return assetPerm;
    }
}
