import { Entity, Property, Collection, OneToMany, IdentifiedReference, ManyToOne, Reference, Cascade, ArrayType, Index } from '@mikro-orm/core';
import { BaseEntity } from '../BaseEntity';
import { ObjectType, Field } from '@nestjs/graphql';
import { AddAssetInput } from './dto/addAsset.args';
import { User } from '../user/user.entity';
import { GraphQLJSON } from 'graphql-type-json';
import { AssetPermission, AssetPermissionType } from './asset_permission.entity';
import { Metadata } from './asset_metadata.embed';
@Entity()
@ObjectType()
export class Asset extends BaseEntity {
    public static not_accessible_message = 'The asset you requested either does not exist, or is not visible to you.';

    @Field()
    @Property()
    name: string;

    @Field((_type) => Asset, { nullable: true })
    @ManyToOne({ entity: () => Asset, eager: true })
    parent?: IdentifiedReference<Asset, 'id'>;

    @Field((_type) => [Asset])
    @OneToMany(() => Asset, (asset) => asset.parent)
    children? = new Collection<Asset>(this);

    @Field((_type) => [AssetPermission])
    @OneToMany(() => AssetPermission, (permission) => permission.asset, { lazy: true, cascade: [Cascade.ALL] })
    permissions? = new Collection<AssetPermission>(this);

    @Field((_type) => User, { nullable: false })
    @ManyToOne({ entity: () => User, eager: true })
    owner: IdentifiedReference<User, 'id'>;

    @Field((_type) => GraphQLJSON, { nullable: false })
    @Property({ type: 'jsonb', nullable: false })
    @Index({ type: 'GIN' })
    metadata: Record<string, Metadata> = {};

    //TODO: unique contraint?
    @Field((_type) => [String], { nullable: false })
    @Property({ type: ArrayType, default: [] })
    @Index({ type: 'GIN' })
    tags: string[];

    constructor(assetInput: AddAssetInput, owner: User, parent?: Asset) {
        super();
        this.name = assetInput.name;
        this.metadata = {};
        this.tags = assetInput.tags ? assetInput.tags : [];
        this.parent = parent ? Reference.create(parent) : undefined;
        this.owner = Reference.create(owner);
    }

    public async has_permission(requester: User, perm: AssetPermissionType) {
        if (this.owner.id === requester.id) {
            return true;
        }
        //TODO: this can be optimized by replacing with maunal query that doesn't order results and just searches for permission that matched
        const permissions = await this.permissions.loadItems();
        if (permissions.some((permission) => permission.target.id === requester.id && permission.permissions.includes(perm))) {
            return true;
        } else {
            return false;
        }
    }
}
