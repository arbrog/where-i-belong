import { Resolver, Query, Args, Mutation, ResolveField, Root, ID } from '@nestjs/graphql';
import { NotFoundException, UseGuards } from '@nestjs/common';
import { AssetService } from './asset.service';
import { Asset } from './asset.entity';
import { GqlAuthGuard } from '../auth/strategies/jwt-gql-auth.guard';
import { AddAssetInput, ModifyAssetInput } from './dto/addAsset.args';
import { CurrentUser } from '../user/user.decorator';
import { User } from '../user/user.entity';
import { AuthenticationError, UserInputError } from 'apollo-server-express';
import GraphQLJSON from 'graphql-type-json';
import { AssetPermission, AssetPermissionType } from './asset_permission.entity';
import { toLowerCaseTags } from './util/tagsUtil';

@Resolver((_of: any) => Asset)
@UseGuards(GqlAuthGuard)
export class AssetResolver {
    constructor(private assetService: AssetService) {}

    @Query((_returns) => Asset)
    async getAsset(@CurrentUser() user: User, @Args('id', { type: () => String }) id: string): Promise<Asset> {
        const asset = await this.assetService.findOne({ id });
        if (await asset.has_permission(user, AssetPermissionType.VIEW)) {
            return asset;
        }
        throw new NotFoundException(Asset.not_accessible_message);
    }

    @Query((_returns) => [Asset])
    async getAssets(@CurrentUser() user: User, @Args('only_owned', { type: () => Boolean, defaultValue: true }) only_owned: boolean): Promise<Asset[]> {
        return this.assetService.searchAssets({ name: '' }, only_owned, user.id);
    }

    @Query((_returns) => [Asset])
    async searchAssets(
        @CurrentUser() user: User,
        @Args('name', { type: () => String }) name: string,
        @Args('only_owned', { type: () => Boolean, defaultValue: true }) only_owned: boolean,
        @Args('metadata', { type: () => GraphQLJSON, nullable: true }) metadata?: Record<string, unknown>,
    ): Promise<Asset[]> {
        return this.assetService.searchAssets({ name, metadata }, only_owned, user.id);
    }

    @Mutation((_returns) => Asset)
    async addAsset(@Args('asset') assetInput: AddAssetInput, @CurrentUser() user: User): Promise<Asset> {
        const parent = await this.assetService.findOne({ id: assetInput.parent });
        if (assetInput.parent && !parent) {
            throw new NotFoundException('Parent Asset not found');
        }
        return this.assetService.addAsset({ ...assetInput, tags: toLowerCaseTags(assetInput.tags) }, user, parent);
    }

    @Mutation((_returns) => Asset)
    async editAsset(@Args('id', { type: () => ID }) id: string, @Args('asset') assetInput: ModifyAssetInput, @CurrentUser() user: User): Promise<Asset> {
        const asset = await this.assetService.findOne({ id });
        if (!asset) {
            throw new NotFoundException('Asset not found');
        }
        if (!(await asset.has_permission(user, AssetPermissionType.EDIT))) {
            throw new AuthenticationError('You do not have edit privledges for this asset');
        }
        const parent = await this.assetService.findOne({ id: assetInput.parent });
        if (assetInput.parent && !parent) {
            throw new NotFoundException('Parent Asset not found');
        } else if (assetInput.parent === id) {
            throw new UserInputError('An asset can not be its own parent');
        }
        if (parent) {
            //check for cycle
            let curAsset = parent;
            let count = 0;
            while (curAsset.parent) {
                if (curAsset.parent.id === id || count > 10 ** 4) {
                    throw new UserInputError('Parent cycles are not allowed');
                }
                curAsset = await curAsset.parent.load();
                count++;
            }
        }

        return this.assetService.modifyAsset(asset, assetInput, user, parent);
    }

    @Mutation((_returns) => Asset)
    async setAssetTags(@Args('id', { type: () => ID }) id: string, @Args('tags', { type: () => [String] }) tags: string[], @CurrentUser() user: User): Promise<Asset> {
        const asset = await this.assetService.findOne({ id });
        if (!asset) {
            throw new NotFoundException('Asset not found');
        }
        if (!(await asset.has_permission(user, AssetPermissionType.EDIT))) {
            throw new AuthenticationError('You do not have edit privledges for this asset');
        }

        return this.assetService.setAssetTags(asset, toLowerCaseTags(tags));
    }

    @Mutation((_returns) => Asset)
    async deleteAsset(@CurrentUser() user: User, @Args('id', { type: () => ID }) id: string): Promise<Asset> {
        const asset = await this.assetService.findOne({ id });
        if (!asset) {
            throw new NotFoundException('Asset not found');
        }
        if (!(await asset.has_permission(user, AssetPermissionType.DELETE))) {
            throw new AuthenticationError('You do not have delete privledges for this asset');
        }
        return this.assetService.deleteAsset(asset);
    }

    @ResolveField((_type) => User)
    async owner(@Root() asset: Asset) {
        return asset.owner.load();
    }

    @ResolveField((_type) => [Asset])
    async children(@CurrentUser() user: User, @Root() asset: Asset) {
        const assets = await asset.children.loadItems();
        const result = assets.reduce(async (memo, asset) => ((await asset.has_permission(user, AssetPermissionType.VIEW)) ? [...(await memo), asset] : memo), [] as any);
        return result;
    }

    //TODO: improve performance of this. Caching/batching? Making front end only get parent lazly rather than eagerly since it get parent when searching?
    @ResolveField((_type) => Asset)
    async parent(@CurrentUser() user: User, @Root() asset: Asset) {
        const parent = await asset?.parent?.load();
        if (!(await parent?.has_permission(user, AssetPermissionType.VIEW))) {
            return undefined;
        }
        return parent;
    }

    @ResolveField((_type) => [AssetPermission])
    async permissions(@Root() asset: Asset) {
        return asset.permissions.loadItems();
    }
}
