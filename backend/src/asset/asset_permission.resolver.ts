import { Resolver, Query, Args, Mutation, ResolveField, Root, ID } from '@nestjs/graphql';
import { NotFoundException, UseGuards } from '@nestjs/common';
import { AssetService } from './asset.service';
import { Asset } from './asset.entity';
import { GqlAuthGuard } from '../auth/strategies/jwt-gql-auth.guard';
import { CurrentUser } from '../user/user.decorator';
import { User } from '../user/user.entity';
import { AssetPermission, AssetPermissionType } from './asset_permission.entity';
import { AssetPermissionService } from './asset_permission.service';
import { UserService } from '../user/user.service';
import { AuthenticationError } from 'apollo-server-express';

@Resolver((_of: any) => AssetPermission)
@UseGuards(GqlAuthGuard)
export class AssetPermissionResolver {
    constructor(private assetPermissionService: AssetPermissionService, private assetService: AssetService, private userService: UserService) {}

    @Query((_returns) => AssetPermission)
    async getAssetPermission(@Args('id', { type: () => ID }) id: string): Promise<AssetPermission> {
        return this.assetPermissionService.findOne({ id });
    }

    @Mutation((_returns) => AssetPermission)
    async upsertAssetPermission(
        @CurrentUser() user: User,
        @Args('asset_id', { type: () => ID }) asset_id: string,
        @Args('target_user', { type: () => ID }) user_id: string,
        @Args('permissions', { type: () => [AssetPermissionType] }) permissions: AssetPermissionType[],
    ): Promise<AssetPermission> {
        const asset = await this.assetService.findOne({ id: asset_id });

        if (asset.owner.id !== user.id) {
            throw new AuthenticationError('Only the owner can edit the permissions this asset');
        }

        const existingPermission = await this.assetPermissionService.findOne({ asset: asset_id, target: user_id });
        if (existingPermission) {
            //update permission
            return this.assetPermissionService.updateAssetPermission(existingPermission, permissions);
        } else {
            if (!asset) {
                throw new NotFoundException('Asset was not found');
            }

            const target = await this.userService.findOne({ id: user_id });
            if (!target) {
                throw new NotFoundException('User was not found');
            }

            return this.assetPermissionService.addAssetPermission(permissions, target, asset);
        }
    }

    @Mutation((_returns) => AssetPermission)
    async deleteAssetPermission(@CurrentUser() user: User, @Args('id', { type: () => ID }) id: string): Promise<AssetPermission> {
        const assetPermission = await this.assetPermissionService.findOne({ id });
        if (!assetPermission) {
            throw new NotFoundException('Permission not found');
        }
        if ((await assetPermission.asset.load()).owner.id !== user.id) {
            throw new AuthenticationError('Only the owner can edit the permissions this asset');
        }
        return this.assetPermissionService.deleteAssetPermission(assetPermission);
    }

    @ResolveField((_type) => User)
    async asset(@Root() assetPerm: AssetPermission) {
        return assetPerm.asset.load();
    }

    @ResolveField((_type) => [Asset])
    async target(@Root() assetPerm: AssetPermission) {
        return assetPerm.target.load();
    }
}
