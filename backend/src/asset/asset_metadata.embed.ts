import { Field, ObjectType, registerEnumType } from '@nestjs/graphql';
import GraphQLJSON from 'graphql-type-json';

export enum AssetMetadataTypes {
    STRING = 'STRING',
    NUMBER = 'NUMBER',
    DATE = 'DATE',
    BOOLEAN = 'BOOLEAN',
    IMAGE = 'IMAGE',
}

registerEnumType(AssetMetadataTypes, {
    name: 'AssetMetadataTypes',
});

@ObjectType()
export class Metadata {
    @Field((_type) => AssetMetadataTypes)
    type!: AssetMetadataTypes;

    @Field((_type) => GraphQLJSON)
    data!: any;

    @Field()
    created_at: Date = new Date();

    @Field()
    updated_at: Date = new Date();
}
