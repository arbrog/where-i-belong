import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@mikro-orm/nestjs';
import { EntityRepository } from '@mikro-orm/postgresql';
import { Asset } from './asset.entity';
import { AddAssetInput, ModifyAssetInput } from './dto/addAsset.args';
import { User } from '../user/user.entity';
import { Reference } from '@mikro-orm/core';
import { AssetMetadataService } from './asset_metadata.service';
import { AssetMetadataTypes } from './asset_metadata.embed';
import { S3Service } from '../storage/s3.service';

@Injectable()
export class AssetService {
    constructor(
        @InjectRepository(Asset)
        private readonly assetRepository: EntityRepository<Asset>,
        private assetMetadataService: AssetMetadataService,
        private s3Service: S3Service,
    ) {}

    async findAll(input?: Record<string, any>): Promise<Asset[]> {
        return this.assetRepository.find(input);
    }

    async findOne(input: Record<string, any>): Promise<Asset | undefined> {
        return this.assetRepository.findOne(input);
    }

    async addAsset(assetInput: AddAssetInput, owner: User, parent?: Asset): Promise<Asset> {
        const asset = new Asset(assetInput, owner, parent);
        await this.assetRepository.persistAndFlush(asset);
        return asset;
    }

    async modifyAsset(asset: Asset, assetInput: ModifyAssetInput, owner: User, parent?: Asset): Promise<Asset> {
        //parent
        if (parent) {
            asset.parent = Reference.create(parent);
        } else if (assetInput.parent === null) {
            asset.parent = undefined;
        }
        //name
        if (assetInput.name) {
            asset.name = assetInput.name;
        }
        //metadata
        if (assetInput.metadata) {
            //TODO: switch to a deep comparrison
            const metadataPrepared = this.assetMetadataService.convertInputToMetadata(assetInput.metadata, asset.metadata);

            //check for fields that no longer exist or have changed type (currently only handles images)
            const changedFields = Object.keys(asset.metadata).filter(
                (fieldName) =>
                    !metadataPrepared.hasOwnProperty(fieldName) ||
                    (metadataPrepared[fieldName].type !== asset.metadata[fieldName].type && asset.metadata[fieldName].type === AssetMetadataTypes.IMAGE),
            );
            const removeHanging = changedFields.map(async (fieldName) => {
                return await this.s3Service.removeObject(asset.metadata[fieldName].data);
            });
            await Promise.all(removeHanging);

            asset.metadata = metadataPrepared;
        }
        await this.assetRepository.persistAndFlush(asset);
        return asset;
    }

    async setAssetTags(asset: Asset, tags: string[]): Promise<Asset> {
        asset.tags = tags;
        await this.assetRepository.persistAndFlush(asset);
        return asset;
    }

    async deleteAsset(asset: Asset): Promise<Asset> {
        await this.assetRepository.removeAndFlush(asset);
        return asset;
    }

    async searchAssets({ name, metadata }: { name: string; metadata?: Record<string, unknown> }, only_owned = true, ownerID: string): Promise<Asset[]> {
        let query = this.assetRepository.createQueryBuilder('e0').leftJoin('permissions', 'ap').orderBy({ updated_at: 'DESC' }).limit(100);
        query = query.where({ name: { $ilike: `%${name}%` } });

        if (only_owned) {
            query = query.andWhere({ owner: { $eq: ownerID } });
        } else {
            query = query.andWhere(`e0.owner_id = ? or (ap.target_id = ? and 'view' = ANY (ap.permissions) )`, [ownerID, ownerID]);
        }
        if (metadata) {
            query = query.andWhere({ metadata: { ...metadata } });
        }

        const res1 = await query.execute();
        return res1.map((a) => this.assetRepository.map(a));
    }
}
