export function toLowerCaseTags(tags: string[]) {
    return tags.map((tag) => tag.toLowerCase());
}
