import { Entity, Enum, IdentifiedReference, ManyToOne, Reference, Unique } from '@mikro-orm/core';
import { BaseEntity } from '../BaseEntity';
import { ObjectType, Field, registerEnumType } from '@nestjs/graphql';
import { User } from '../user/user.entity';
import { Asset } from './asset.entity';

@Entity()
@ObjectType()
@Unique({ properties: ['asset', 'target'] })
export class AssetPermission extends BaseEntity {
    @Field((_type) => Asset)
    @ManyToOne({ entity: () => Asset, eager: true })
    asset: IdentifiedReference<Asset, 'id'>;

    @Field((_type) => User, { nullable: false })
    @ManyToOne({ entity: () => User, eager: true })
    target: IdentifiedReference<User, 'id'>;

    @Field((_type) => [AssetPermissionType])
    @Enum({ items: () => AssetPermissionType, array: true })
    permissions: AssetPermissionType[] = [];

    constructor(target: User, asset: Asset, permissions: AssetPermissionType[]) {
        super();
        this.asset = Reference.create(asset);
        this.target = Reference.create(target);
        this.permissions = permissions;
    }
}

export enum AssetPermissionType {
    VIEW = 'view',
    EDIT = 'edit',
    DELETE = 'delete',
}

registerEnumType(AssetPermissionType, {
    name: 'AssetPermissionType',
});
