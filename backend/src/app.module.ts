import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from '@nestjs/config';
import { databaseConfig } from './database/database.config';
import { DatabaseModule } from './database/database.module';
import { UserModule } from './user/user.module';
import { join } from 'path';
import { GraphQLJSON } from 'graphql-type-json';
import { GraphQLModule } from '@nestjs/graphql';
import { AuthModule } from './auth/auth.module';
import { AssetModule } from './asset/asset.module';
import { StorageModule } from './storage/storage.module';
import { s3Config } from './storage/s3.config';
import { featureFlagConfig } from './feature.config';

@Module({
    imports: [
        ConfigModule.forRoot({
            cache: true,
            isGlobal: true,
            load: [databaseConfig, s3Config, featureFlagConfig],
            envFilePath: process.env.NODE_ENV === 'production' ? ['.env.production', '.env.production.local'] : ['.env', '.env.local'],
        }),
        GraphQLModule.forRoot({
            resolvers: { JSON: GraphQLJSON },
            autoSchemaFile: join(process.cwd(), 'src/schema.gql'),
            context: ({ req }) => ({ req }),
            introspection: true,
            tracing: true,
            cors: true,
        }),
        DatabaseModule.forRoot(),
        UserModule,
        AuthModule,
        AssetModule,
        StorageModule,
    ],
    controllers: [AppController],
    providers: [AppService],
})
export class AppModule {}
