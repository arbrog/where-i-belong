import { PrimaryKey, Property, Index, AnyEntity } from '@mikro-orm/core';
import { Field, ID, ObjectType } from '@nestjs/graphql';
import { v4 } from 'uuid';

@ObjectType()
export abstract class BaseEntity implements AnyEntity<BaseEntity> {
    @Field((_type) => ID)
    @PrimaryKey({ columnType: 'uuid' })
    id = v4();

    @Field()
    @Property({ columnType: 'timestamptz(3)' })
    @Index()
    created_at: Date = new Date();

    @Field()
    @Property({ onUpdate: () => new Date(), columnType: 'timestamptz(3)' })
    updated_at: Date = new Date();
}
