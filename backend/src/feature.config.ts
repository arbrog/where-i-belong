import { registerAs } from '@nestjs/config';

function convertStringToBool(value?: string) {
    return (value || '').toLowerCase() === 'true' ? true : false;
}

//TODO: change dependiecies to use dynamic modules rather than if statements in resolvers
//TODO: add joi validation
export const featureFlagConfig = registerAs('features', () => ({
    accountCreation: process.env.ACCOUNT_CREATION ? convertStringToBool(process.env.ACCOUNT_CREATION) : true,
    passwordResets: convertStringToBool(process.env.PASSWORD_RESET) || false,
}));
