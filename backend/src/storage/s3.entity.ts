import { ObjectType, Field } from '@nestjs/graphql';

@ObjectType()
export class Image {
    @Field()
    name: string;

    @Field()
    url: string;

    @Field()
    createdAt: Date;

    @Field()
    expirationAt: Date;

    public static not_accessible_message = 'The image you requested either does not exist, or is not visible to you.';
}
