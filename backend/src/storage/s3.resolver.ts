import { Resolver, Query, Args, Mutation } from '@nestjs/graphql';
import { UseGuards } from '@nestjs/common';
import { GqlAuthGuard } from '../auth/strategies/jwt-gql-auth.guard';
import { CurrentUser } from '../user/user.decorator';
import { User } from '../user/user.entity';
import { S3Service } from './s3.service';
import { Image } from './s3.entity';

@Resolver((_of: any) => Image)
@UseGuards(GqlAuthGuard)
export class S3Resolver {
    constructor(private s3Service: S3Service) {}

    //TODO: add asset permission check

    @Query((_returns) => Image)
    async getImageURI(@CurrentUser() user: User, @Args('name', { type: () => String }) name: string): Promise<Image> {
        const uriResp = await this.s3Service.getPresignedURI(name);
        const currentDateTime = new Date();
        return { name, url: uriResp.URI, createdAt: currentDateTime, expirationAt: new Date(currentDateTime.getTime() + uriResp.expiration * 1000) };
        // if (await asset.has_permission(user, AssetPermissionType.VIEW)) {
        //     return uri;
        // }
        // throw new NotFoundException(Image.not_accessible_message);
    }

    @Mutation((_returns) => String)
    async putImageURI(@CurrentUser() user: User, @Args('name', { type: () => String }) name: string): Promise<string> {
        const uriResp = await this.s3Service.putPresignedURI(name);
        return uriResp.URI;
        // if (await asset.has_permission(user, AssetPermissionType.VIEW)) {
        //     return uri;
        // }
        // throw new NotFoundException(Image.not_accessible_message);
    }

    @Mutation((_returns) => Boolean)
    async removeImage(@CurrentUser() user: User, @Args('name', { type: () => String }) name: string): Promise<boolean> {
        await this.s3Service.removeObject(name);
        return true;
        // if (await asset.has_permission(user, AssetPermissionType.VIEW)) {
        //     return uri;
        // }
        // throw new NotFoundException(Image.not_accessible_message);
    }
}
