import { registerAs } from '@nestjs/config';

function convertStringToBool(value?: string) {
    return (value || '').toLowerCase() === 'true' ? true : false;
}

//TODO: add joi validation
export const s3Config = registerAs('s3', () => ({
    enabled: convertStringToBool(process.env.S3_ENABLED),
    host: process.env.S3_HOSTNAME,
    port: parseInt(process.env.S3_PORT, 10),
    ssl: convertStringToBool(process.env.S3_SSL),
    accessKey: process.env.S3_ACCESS_KEY,
    secretKey: process.env.S3_SECRET_KEY,
    imageBucket: process.env.S3_IMAGE_BUCKET || 'imageBucket',
}));
