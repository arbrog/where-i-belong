import { Inject, Injectable } from '@nestjs/common';
import { ConfigType } from '@nestjs/config';
import { Client as MinioClient } from 'minio';
import { s3Config } from './s3.config';

// TODO: add policy for file type and size

@Injectable()
export class S3Service {
    constructor(
        @Inject(s3Config.KEY)
        private s3ConfigEnv: ConfigType<typeof s3Config>,
    ) {}
    private readonly s3Client = this.s3ConfigEnv.enabled
        ? new MinioClient({
              endPoint: this.s3ConfigEnv.host,
              port: this.s3ConfigEnv.port,
              useSSL: this.s3ConfigEnv.ssl,
              accessKey: this.s3ConfigEnv.accessKey,
              secretKey: this.s3ConfigEnv.secretKey,
          })
        : null;

    async getPresignedURI(objectName: string, expiry = 60 * 60 * 8) {
        const URI = await this.s3Client.presignedGetObject(this.s3ConfigEnv.imageBucket, objectName, expiry);
        return { URI, expiration: expiry };
    }

    async putPresignedURI(objectName: string, expiry = 60 * 60 * 1) {
        const URI = await this.s3Client.presignedPutObject(this.s3ConfigEnv.imageBucket, objectName, expiry);
        return { URI, expiration: expiry };
    }

    async removeObject(objectName: string) {
        await this.s3Client.removeObject(this.s3ConfigEnv.imageBucket, objectName);
    }

    async bucketExists(bucketName: string) {
        return this.s3Client.bucketExists(bucketName);
    }

    async makeBucket(bucketName: string, region?: string) {
        return this.s3Client.makeBucket(bucketName, region || 'us-east-1');
    }

    async getBucketPolicy(bucketName: string) {
        return this.s3Client.getBucketPolicy(bucketName);
    }

    async setBucketPolicy(bucketName: string, policy?: Record<string, any>) {
        return this.s3Client.setBucketPolicy(bucketName, JSON.stringify(policy));
    }
}
