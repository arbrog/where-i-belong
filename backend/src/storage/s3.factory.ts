import { S3Service } from './s3.service';
import { ConfigService, ConfigType } from '@nestjs/config';
import { s3Config } from './s3.config';

// const generateDefaultPolicy = (bucketName: string) => ({
//     Version: '2012-10-17',
//     Statement: [
//         {
//             Sid: 'readonly',
//             Effect: 'Allow',
//             Action: ['s3:GetObject'],
//             Resource: `arn:aws:s3:::${bucketName}/*`,
//         },
//         {
//             Sid: 'readonly',
//             Effect: 'Allow',
//             Action: ['s3:GetBucketPolicy', 's3:HeadBucket', 's3:ListBucket'],
//             Resource: `arn:aws:s3:::${bucketName}`,
//         },
//     ],
// });

//TODO: add AWS S3 flag and policy setting (done manually currently with s3 policy)
export const S3Factory = {
    provide: S3Service,
    useFactory: async (configService: ConfigService) => {
        const config = configService.get<ConfigType<typeof s3Config>>('s3');
        const s3Service = new S3Service(config);
        if (config.enabled) {
            const bucketName = configService.get('s3.imageBucket');
            const bucketExists = await s3Service.bucketExists(bucketName);
            if (!bucketExists) {
                await s3Service.makeBucket(bucketName);
            }
            // const policy = s3Service.getBucketPolicy(bucketName);
            // await s3Service.setBucketPolicy(bucketName, generateDefaultPolicy(bucketName));
        }

        return s3Service;
    },
    inject: [ConfigService],
};
