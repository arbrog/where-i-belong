import { Module } from '@nestjs/common';
import { S3Factory } from './s3.factory';
import { S3Resolver } from './s3.resolver';

@Module({
    controllers: [],
    providers: [S3Factory, S3Resolver],
    exports: [S3Factory],
})
export class StorageModule {}
