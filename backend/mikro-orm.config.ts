import { Connection, IDatabaseDriver, Options } from '@mikro-orm/core';
import { TsMorphMetadataProvider } from '@mikro-orm/reflection';
import { Asset } from './src/asset/asset.entity';
import { AssetPermission } from './src/asset/asset_permission.entity';
import { BaseEntity } from './src/BaseEntity';
import { Token } from './src/user/token.entity';
import { User } from './src/user/user.entity';
// eslint-disable-next-line @typescript-eslint/no-var-requires
require('dotenv').config();

const baseOpts: Partial<Options<IDatabaseDriver<Connection>>> = {
    entities: [BaseEntity, User, Asset, AssetPermission, Token],
    metadataProvider: TsMorphMetadataProvider,
    type: 'postgresql',
    migrations: {
        disableForeignKeys: false,
        dropTables: false,
        safe: false,
        transactional: true,
        allOrNothing: true,
        emit: 'ts',
    },
    // pool: {
    //     idleTimeoutMillis: 100,
    //     min: 0,
    // },
};

let extraOpts: Partial<Options<IDatabaseDriver<Connection>>> = {};
if (process.env.DATABASE_HOSTNAME) {
    extraOpts = {
        host: process.env.DATABASE_HOSTNAME,
        port: process.env.DATABASE_PORT && parseInt(process.env.DATABASE_PORT),
        password: process.env.DATABASE_PASSWORD,
        user: process.env.DATABASE_USER,
        dbName: process.env.DATABASE_NAME,
    };
} else {
    throw new Error('Missing env variables');
}

export default {
    ...baseOpts,
    ...extraOpts,
};
