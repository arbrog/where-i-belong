import { Migration } from '@mikro-orm/migrations';

export class Migration20210807191208 extends Migration {
    async up(): Promise<void> {
        this.addSql('alter table "token" drop constraint if exists "token_scope_check";');
        this.addSql('alter table "token" alter column "scope" type text using ("scope"::text);');
        this.addSql('alter table "token" add constraint "token_scope_check" check ("scope" in (\'password_reset\'));');
    }
}
