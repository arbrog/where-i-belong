import { Migration } from '@mikro-orm/migrations';

export class Migration20210807190731 extends Migration {
    async up(): Promise<void> {
        this.addSql(
            'create table "user" ("id" uuid not null, "created_at" timestamptz(3) not null, "updated_at" timestamptz(3) not null, "first_name" varchar(255) not null, "last_name" varchar(255) not null, "email" varchar(255) not null, "password" varchar(255) null, "verified" bool not null);',
        );
        this.addSql('alter table "user" add constraint "user_pkey" primary key ("id");');
        this.addSql('create index "user_created_at_index" on "user" ("created_at");');
        this.addSql('create index "user_email_index" on "user" ("email");');
        this.addSql('alter table "user" add constraint "user_email_unique" unique ("email");');

        this.addSql(
            'create table "asset" ("id" uuid not null, "created_at" timestamptz(3) not null, "updated_at" timestamptz(3) not null, "name" varchar(255) not null, "parent_id" uuid null, "owner_id" uuid not null, "metadata" jsonb not null, "tags" text[] not null default \'{}\');',
        );
        this.addSql('alter table "asset" add constraint "asset_pkey" primary key ("id");');
        this.addSql('create index "asset_created_at_index" on "asset" ("created_at");');

        this.addSql(
            'create table "asset_permission" ("id" uuid not null, "created_at" timestamptz(3) not null, "updated_at" timestamptz(3) not null, "asset_id" uuid not null, "target_id" uuid not null, "permissions" text[] not null);',
        );
        this.addSql('alter table "asset_permission" add constraint "asset_permission_pkey" primary key ("id");');
        this.addSql('create index "asset_permission_created_at_index" on "asset_permission" ("created_at");');

        this.addSql(
            'create table "token" ("id" uuid not null, "created_at" timestamptz(3) not null, "updated_at" timestamptz(3) not null, "ttl" int4 not null, "scope" text check ("scope" in (\'password_reset\')) not null, "token" varchar(255) not null, "consumed" bool not null, "user_id" uuid not null);',
        );
        this.addSql('alter table "token" add constraint "token_pkey" primary key ("id");');
        this.addSql('create index "token_created_at_index" on "token" ("created_at");');
        this.addSql('alter table "token" add constraint "token_token_unique" unique ("token");');

        this.addSql('alter table "asset" add constraint "asset_parent_id_foreign" foreign key ("parent_id") references "asset" ("id") on update cascade on delete set null;');
        this.addSql('alter table "asset" add constraint "asset_owner_id_foreign" foreign key ("owner_id") references "user" ("id") on update cascade;');

        this.addSql('alter table "asset_permission" add constraint "asset_permission_asset_id_foreign" foreign key ("asset_id") references "asset" ("id") on update cascade;');
        this.addSql('alter table "asset_permission" add constraint "asset_permission_target_id_foreign" foreign key ("target_id") references "user" ("id") on update cascade;');

        this.addSql('alter table "token" add constraint "token_user_id_foreign" foreign key ("user_id") references "user" ("id") on update cascade;');

        this.addSql('create index "asset_metadata_index" on "asset" using GIN ("metadata");');
        this.addSql('create index "asset_tags_index" on "asset" using GIN ("tags");');

        this.addSql('alter table "asset_permission" add constraint "asset_permission_asset_id_target_id_unique" unique ("asset_id", "target_id");');
    }
}
