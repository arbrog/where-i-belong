import http from "k6/http";
import { sleep } from "k6";

export let options = {
  stages: [
    { duration: "15s", target: 50 },
    { duration: "30s", target: 500 },
    { duration: "15s", target: 50 },
  ],
};

// export const options = {
//   vus: 80,
//   duration: "30s",
// };

const TOKEN = "";

const params = {
  headers: {
    "Content-Type": "application/json",
    Authorization: `Bearer ${TOKEN}`,
  },
};

let query = `
    query search_assets {
        searchAssets(name: "T", only_owned: false) {
            id
            name
            metadata
            owner {
                id
                firstName
            }
            created_at
            updated_at
        }
    }`;

export default function () {
  let res = http.post(
    "https://api.wib.austin-brogan.com/graphql",
    JSON.stringify({ query: query }),
    params
  );
  //   console.log(res.status)
  //   if (res.status === 200) {
  //     console.log(JSON.stringify(res.body));
  //   }
  sleep(1);
}
